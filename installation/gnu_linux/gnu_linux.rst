.. index::
   pair: Python ; GNU/Linux

.. _python_gnu:

==================================
GNU/Linux Python installation
==================================

.. seealso::

   - https://testdriven.io/blog/python-environments/

.. toctree::
   :maxdepth: 6

   pyenv/pyenv
