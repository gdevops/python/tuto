.. index::
   pair: Python ; pyenv

.. _pyenv:

==========================================
**Python installation with pyenv**
==========================================

.. seealso::

   - https://github.com/pyenv/pyenv
   - https://realpython.com/intro-to-pyenv/
   - https://www.tecmint.com/pyenv-install-and-manage-multiple-python-versions-in-linux/
   - https://cjolowicz.github.io/posts/hypermodern-python-01-setup/
   - https://testdriven.io/blog/python-environments/
   - https://blog.stephane-robert.info/post/python-pyenv-pipenv/



.. toctree::
   :maxdepth: 3

   installation/installation
   commands/commands
   examples/examples
   versions/versions
