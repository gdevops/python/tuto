.. index::
   pair: pyenv; installation

.. _pyenv_installation:

==========================================
**pyenv installation**
==========================================

- :ref:`poetry_installation`

Dependencies
==============

- https://blog.stephane-robert.info/post/python-pyenv-pipenv/
- https://devguide.python.org/getting-started/setup-building/index.html#install-dependencies


**These dependencies are necessary, because pyenv compiles the target Python
version from the sources**.


Debian/Ubuntu
----------------

::

    sudo apt-get install build-essential gdb lcov pkg-config \
      libbz2-dev libffi-dev libgdbm-dev libgdbm-compat-dev liblzma-dev \
      libncurses5-dev libreadline6-dev libsqlite3-dev libssl-dev \
      lzma lzma-dev tk-dev uuid-dev zlib1g-dev

.. _python_pyenv_installation:

Installation with git
=======================


::

    git clone https://github.com/pyenv/pyenv.git ~/.pyenv

::

    Clonage dans '/home/pvergain/.pyenv'...
    remote: Enumerating objects: 56, done.
    remote: Counting objects: 100% (56/56), done.
    remote: Compressing objects: 100% (33/33), done.
    remote: Total 16616 (delta 20), reused 43 (delta 17), pack-reused 16560
    Réception d'objets: 100% (16616/16616), 3.24 MiB | 2.17 MiB/s, fait.
    Résolution des deltas: 100% (11252/11252), fait.


Update ~/.bash_profile, ~/.zshenv and ~/.zshrc
=================================================


::

    echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bash_profile
    echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshenv

    echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bash_profile
    echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshenv
