.. index::
   pair: command ; pyenv


.. _pyenv_command:

===========================
pyenv versions
===========================





pyenv 1.2.23-18-g16d0f50
============================

::

    pyenv 1.2.15-11-g4e0ba2f4

::

    Usage: pyenv <command> [<args>]

    Some useful pyenv commands are:
       activate    Activate virtual environment
       commands    List all available pyenv commands
       deactivate   Deactivate virtual environment
       doctor      Verify pyenv installation and development tools to build pythons.
       exec        Run an executable with the selected Python version
       global      Set or show the global Python version(s)
       help        Display help for a command
       hooks       List hook scripts for a given pyenv command
       init        Configure the shell environment for pyenv
       install     Install a Python version using python-build
       local       Set or show the local application-specific Python version(s)
       prefix      Display prefix for a Python version
       rehash      Rehash pyenv shims (run this after installing executables)
       root        Display the root directory where versions and shims are kept
       shell       Set or show the shell-specific Python version
       shims       List existing pyenv shims
       uninstall   Uninstall a specific Python version
       --version   Display the version of pyenv
       version     Show the current Python version(s) and its origin
       version-file   Detect the file that sets the current pyenv version
       version-name   Show the current Python version
       version-origin   Explain how the current Python version is set
       versions    List all Python versions available to pyenv
       virtualenv   Create a Python virtualenv using the pyenv-virtualenv plugin
       virtualenv-delete   Uninstall a specific Python virtualenv
       virtualenv-init   Configure the shell environment for pyenv-virtualenv
       virtualenv-prefix   Display real_prefix for a Python virtualenv version
       virtualenvs   List all Python virtualenvs found in `$PYENV_ROOT/versions/*'.
       whence      List all Python versions that contain the given executable
       which       Display the full path to an executable

    See `pyenv help <command>' for information on a specific command.
    For full documentation, see: https://github.com/pyenv/pyenv#readme




pyenv 1.2.15-11-g4e0ba2f4
============================

::

    pyenv

::

    pyenv 1.2.15-11-g4e0ba2f4
    Usage: pyenv <command> [<args>]

    Some useful pyenv commands are:
       commands    List all available pyenv commands
       exec        Run an executable with the selected Python version
       global      Set or show the global Python version
       help        Display help for a command
       hooks       List hook scripts for a given pyenv command
       init        Configure the shell environment for pyenv
       install     Install a Python version using python-build
       local       Set or show the local application-specific Python version
       prefix      Display prefix for a Python version
       rehash      Rehash pyenv shims (run this after installing executables)
       root        Display the root directory where versions and shims are kept
       shell       Set or show the shell-specific Python version
       shims       List existing pyenv shims
       uninstall   Uninstall a specific Python version
       version     Show the current Python version and its origin
       --version   Display the version of pyenv
       version-file   Detect the file that sets the current pyenv version
       version-name   Show the current Python version
       version-origin   Explain how the current Python version is set
       versions    List all Python versions available to pyenv
       whence      List all Python versions that contain the given executable
       which       Display the full path to an executable

    See `pyenv help <command>' for information on a specific command.
    For full documentation, see: https://github.com/pyenv/pyenv#readme




pyenv 1.2.9-6-gddb335c1
=========================


::

    pvergain@pvergain-MS-7721 > pyenv

::

    pyenv 1.2.9-6-gddb335c1
    Usage: pyenv <command> [<args>]

    Some useful pyenv commands are:
       commands    List all available pyenv commands
       local       Set or show the local application-specific Python version
       global      Set or show the global Python version
       shell       Set or show the shell-specific Python version
       install     Install a Python version using python-build
       uninstall   Uninstall a specific Python version
       rehash      Rehash pyenv shims (run this after installing executables)
       version     Show the current Python version and its origin
       versions    List all Python versions available to pyenv
       which       Display the full path to an executable
       whence      List all Python versions that contain the given executable

    See `pyenv help <command>' for information on a specific command.
    For full documentation, see: https://github.com/pyenv/pyenv#readme

::

    pvergain@pvergain-MS-7721 > pyenv versions

::

    * system (set by /home/pvergain/.pyenv/version)
