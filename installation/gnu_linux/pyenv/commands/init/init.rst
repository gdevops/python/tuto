
.. _pyenv_init:

============
pyenv init
============

::

    pyenv init

::

    # Load pyenv automatically by appending
    # the following to your profile:

    eval "$(pyenv init -)"


::

    echo 'eval "$(pyenv init -)"' >> ~/.zshrc
    echo 'export PIPENV_VENV_IN_PROJECT=1' >> ~/.zshrc
