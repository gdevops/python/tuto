.. index::
   pair: commands ; pyenv

.. _pyenv_commands:

==========================================
commands
==========================================


.. toctree::
   :maxdepth: 3

   init/init
   install/install
   update/update
