
.. _pyenv_update:

==========================================
pyenv update
==========================================

::

    pyenv update


Example::

    ✦ ❯ pyenv update
    Updating /home/pvergain/.pyenv...
    remote: Enumerating objects: 224, done.
    remote: Counting objects: 100% (181/181), done.
    remote: Compressing objects: 100% (32/32), done.
    remote: Total 88 (delta 57), reused 78 (delta 49), pack-reused 0
    Dépaquetage des objets: 100% (88/88), 20.89 Kio | 324.00 Kio/s, fait.
    Depuis https://github.com/pyenv/pyenv
     * branch              master     -> FETCH_HEAD
     * [nouvelle étiquette] v2.2.5     -> v2.2.5
       3b0ee099..986fe1a7  master     -> origin/master
    Mise à jour 3b0ee099..986fe1a7
    Fast-forward
     CHANGELOG.md                                                                                                                         |  18 ++++-
     COMMANDS.md                                                                                                                          |   6 +-
     README.md                                                                                                                            |   2 +-
     libexec/pyenv---version                                                                                                              |   2 +-
     libexec/pyenv-prefix                                                                                                                 |  10 +--
     plugins/python-build/share/python-build/3.10-dev                                                                                     |   2 +-
     plugins/python-build/share/python-build/3.10.0                                                                                       |   2 +-
     plugins/python-build/share/python-build/3.10.1                                                                                       |   2 +-
     plugins/python-build/share/python-build/3.10.2                                                                                       |   2 +-
     plugins/python-build/share/python-build/3.10.3                                                                                       |  10 +++
     plugins/python-build/share/python-build/3.11.0a6                                                                                     |   2 +-
     plugins/python-build/share/python-build/3.7.13                                                                                       |  10 +++
     plugins/python-build/share/python-build/3.8.13                                                                                       |  10 +++
     plugins/python-build/share/python-build/3.9.11                                                                                       |  10 +++
     plugins/python-build/share/python-build/miniconda3-3.7-4.11.0                                                                        |  19 +++++
     plugins/python-build/share/python-build/miniconda3-3.8-4.11.0                                                                        |  19 +++++
     plugins/python-build/share/python-build/miniconda3-3.9-4.11.0                                                                        |  19 +++++
     plugins/python-build/share/python-build/patches/3.7.13/Python-3.7.13/0001-Port-ctypes-and-system-libffi-patches-for-arm64-macO.patch | 310 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     plugins/python-build/share/python-build/patches/3.7.13/Python-3.7.13/0002-bpo-41100-fix-_decimal-for-arm64-Mac-OS-GH-21228.patch     |  36 +++++++++
     plugins/python-build/share/python-build/patches/3.7.13/Python-3.7.13/0003-bpo-42351-Avoid-error-when-opening-header-with-non-U.patch |  30 +++++++
     plugins/python-build/share/python-build/pypy2.7-7.3.8                                                                                |  81 +++++++++++++++++++
     plugins/python-build/share/python-build/pypy2.7-7.3.8-src                                                                            |  14 ++++
     plugins/python-build/share/python-build/pypy3.7-7.3.7-src                                                                            |   2 +-
     plugins/python-build/share/python-build/pypy3.7-7.3.8                                                                                |  81 +++++++++++++++++++
     plugins/python-build/share/python-build/pypy3.7-7.3.8-src                                                                            |  14 ++++
     plugins/python-build/share/python-build/pypy3.8-7.3.8                                                                                |  81 +++++++++++++++++++
     plugins/python-build/share/python-build/pypy3.8-7.3.8-src                                                                            |  14 ++++
     plugins/python-build/share/python-build/pypy3.9-7.3.8                                                                                |  81 +++++++++++++++++++
     plugins/python-build/share/python-build/pypy3.9-7.3.8-src                                                                            |  14 ++++
     29 files changed, 885 insertions(+), 18 deletions(-)
     create mode 100644 plugins/python-build/share/python-build/3.10.3
     create mode 100644 plugins/python-build/share/python-build/3.7.13
     create mode 100644 plugins/python-build/share/python-build/3.8.13
     create mode 100644 plugins/python-build/share/python-build/3.9.11
     create mode 100644 plugins/python-build/share/python-build/miniconda3-3.7-4.11.0
     create mode 100644 plugins/python-build/share/python-build/miniconda3-3.8-4.11.0
     create mode 100644 plugins/python-build/share/python-build/miniconda3-3.9-4.11.0
     create mode 100644 plugins/python-build/share/python-build/patches/3.7.13/Python-3.7.13/0001-Port-ctypes-and-system-libffi-patches-for-arm64-macO.patch
     create mode 100644 plugins/python-build/share/python-build/patches/3.7.13/Python-3.7.13/0002-bpo-41100-fix-_decimal-for-arm64-Mac-OS-GH-21228.patch
     create mode 100644 plugins/python-build/share/python-build/patches/3.7.13/Python-3.7.13/0003-bpo-42351-Avoid-error-when-opening-header-with-non-U.patch
     create mode 100644 plugins/python-build/share/python-build/pypy2.7-7.3.8
     create mode 100644 plugins/python-build/share/python-build/pypy2.7-7.3.8-src
     create mode 100644 plugins/python-build/share/python-build/pypy3.7-7.3.8
     create mode 100644 plugins/python-build/share/python-build/pypy3.7-7.3.8-src
     create mode 100644 plugins/python-build/share/python-build/pypy3.8-7.3.8
     create mode 100644 plugins/python-build/share/python-build/pypy3.8-7.3.8-src
     create mode 100644 plugins/python-build/share/python-build/pypy3.9-7.3.8
     create mode 100644 plugins/python-build/share/python-build/pypy3.9-7.3.8-src
    Updating /home/pvergain/.pyenv/plugins/pyenv-doctor...
    Depuis https://github.com/pyenv/pyenv-doctor
     * branch            master     -> FETCH_HEAD
    Déjà à jour.
    Updating /home/pvergain/.pyenv/plugins/pyenv-installer...
    remote: Enumerating objects: 7, done.
    remote: Counting objects: 100% (7/7), done.
    remote: Compressing objects: 100% (3/3), done.
    remote: Total 3 (delta 2), reused 0 (delta 0), pack-reused 0
    Dépaquetage des objets: 100% (3/3), 718 octets | 47.00 Kio/s, fait.
    Depuis https://github.com/pyenv/pyenv-installer
     * branch            master     -> FETCH_HEAD
       4164fce..771de8e  master     -> origin/master
    Mise à jour 4164fce..771de8e
    Fast-forward
     README.rst | 2 ++
     1 file changed, 2 insertions(+)
    Updating /home/pvergain/.pyenv/plugins/pyenv-update...
    Depuis https://github.com/pyenv/pyenv-update
     * branch            master     -> FETCH_HEAD
    Déjà à jour.
    Updating /home/pvergain/.pyenv/plugins/pyenv-virtualenv...
    Depuis https://github.com/pyenv/pyenv-virtualenv
     * branch            master     -> FETCH_HEAD
    Déjà à jour.
    Updating /home/pvergain/.pyenv/plugins/pyenv-which-ext...
    Depuis https://github.com/pyenv/pyenv-which-ext
     * branch            master     -> FETCH_HEAD
    Déjà à jour.
