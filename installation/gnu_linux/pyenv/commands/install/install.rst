
.. _pyenv_install:

==========================================
pyenv install
==========================================

::

    pyenv install --list | grep "3\.10"

::

    3.10.0a7
    3.10.0b1
    3.10-dev
    miniconda-3.10.1
    miniconda3-3.10.1
