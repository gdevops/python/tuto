.. index::
   pair: Python ; Installation

.. _python_install:

==================================
Installation
==================================


.. toctree::
   :maxdepth: 6

   docker/docker
   gnu_linux/gnu_linux
