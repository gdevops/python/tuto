.. index::
   pair: Python ; rich
   ! rich

.. _python_rich:

==================================================================================
**rich** (a Python library for rich text and beautiful formatting in the terminal)
==================================================================================

.. seealso::

   - https://github.com/willmcgugan/rich
   - https://www.willmcgugan.com/tag/rich/


.. toctree::
   :maxdepth: 3

   versions/versions
