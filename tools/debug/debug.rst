.. index::
   pair: Python ; debug
   pair: breakpoint() ; debug

.. _python_debug:

==================================
Python debug tips
==================================



Instead of: "import pdb; pdb.set_trace()" Do this: "breakpoint()" by Nina Zakharenko
=====================================================================================


- https://www.youtube.com/watch?v=y_dCT9TQtyQ
- https://www.nnja.io/post/2019/djangocon-2019-goodbye-print-hello-debugger/


::

    Instead of: "import pdb; pdb.set_trace()"
    Do this: "breakpoint()"
    It's available in Python 3.7+.
    Want a fancier debugger?
    export PYTHONBREAKPOINT=ipdb.set_trace
    Want to disable all breakpoint?
    export PYTHONBREAKPOINT=0



- https://x.com/jpetazzo/status/1236299987820167168?s=20

::

    Also, when you're in the Python debugger and want a "real" Python prompt, use "interact"
    Her talk also includes nice demos of debugging web apps (including templates and code in templates) with IDEs like VS Code.
    I'm glad I took the time to watch this!
