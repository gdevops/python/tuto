.. index::
   pair: Python ; pyinspect
   ! pyinspect

.. _pyinspect:

==================================================================================
**pyinspect** (find functions when you can't remember their name)
==================================================================================

.. seealso::

   - https://github.com/FedeClaudi/pyinspect
   - https://x.com/Federico_claudi
   - https://x.com/Federico_claudi/status/1312774985036029953?s=20
   - https://github.com/FedeClaudi/pyinspect#tracebacks
   - https://scidraw.io/
   - https://x.com/scidraw



Annouce
=========

.. seealso::

   - https://x.com/Federico_claudi/status/1312774985036029953?s=20
   - https://x.com/Federico_claudi/status/1312774993776988160?s=20

Just released "pyinspect", the #python package for lazy programmers.

Use pyinspect to:

- find functions by their name (without having to google it)
- and print a functions code to terminal (without having to open its file)


pyinspect was only possible because of the amazing "rich" library from
@willmcgugan : one of the coolest open source libraries I've come across.

If you get a chance, check out @willmcgugan's work (and support him!):


Description
================

If, like me, when coding often you know which function you need but can't
quite remember its name, then I have good news!. pyinspect is here to help you out.

pyinspect allows you to search for functions and class methods based on
their name and prints out a handy table with all the functions that met
your search criteria.

You can also use pyinspect to print a function's code directly in your
terminal so that you can remind yourself what it does without having to
open any file!

Installing pyinspect
=======================

It's as simple as::

    pip install pyinspect

Finding functions
====================

The easiest way to grasp how pyinspect can help is with an example.

Imagine that you just can't remember which matplotlib.pyplot method
you need to create a figure with subplots...

this is how you use pyinspect to find the function you need::

    # import the module whose functions you're looking for
    import matplotlib.pyplot as plt

    # import pyinspect
    import pyinspect

    # Find the functions you're looking for
    funcs = pyinspect.search(plt, name='subplot')

This results in a table with all the function's matching your search name:

pyinspect.find can also be used to find class attributes. For example,
say that you're looking for a method with export in the name in rich.console.Console::

    # import the class you need to inspect
    from rich.console import Console

    # import pyinspect
    import pyinspect

    # find class methods
    methods = pyinspect.search(Console, 'export')

Inspecting functions
========================

Okay, you've found the function you need, that's great. But how does it work?

What if, in addition to the exact name, you've forgotten which arguments
it takes, or what it does exactly.

Well, pyinspect can help you there as well!

You can use pyinspect.print_function to print the source code of any
function or attribute directly in your terminal.

This way you can inspect what the function does without having to
open any file!

This is how to do it::

    # import pyinspect
    import pyinspect as pi

    # Look at how pyinspect.search works
    pi.showme(pi.search)


Tracebacks
===========

Finally, pyinspect builds upon rich's awesome traceback functionality
to print nicely formatted tracebacks and show the local variables when
the exception came up.

E.g.::

    # import pyinspect and install the traceback handler
    import pi

    pi.install_traceback()  # use hide_locals=True to hide locals panels

    # make some buggy code
    import numpy as np

    a = np.ones(5)
    b = "ignore this"  # a local variable not being used
    c = np.zeros(4)  # ooops, wrong size

    a + c  # this will give an error


pro tips
---------

if you want to show all items in the local scope (e.g. also imported
modules, not just variables) then you can use all_locals=True in pi.install_traceback()
if you don't want the locals to be shown at all, then use hide_locals=True
if you want more or less extensive tracebacks, you can use keep_frames
to decide how many frames to shown in nested tracebacks (i.e when a function
a calls a function b and the error comes up in b, do you want to see
only the locals in b or in a and b?)

Acknowledgements
===================

pyinspect is mostly a thin wrapper on top of the awesome rich library,
so a huge thank you goes to @willmcgugan for the great job done with rich.
