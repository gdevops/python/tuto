"""Transforme une image codée en base64
   en image au format PNG

    .
    ├── decode_base64.py
    ├── images
    │   ├── base_64_cca.png
    │   ├── base_64_cca.txt
    │   ├── base_64_galgadot.png
    │   ├── base_64_galgadot.txt
    │   ├── base_64_hemsworh_1.png
    │   ├── base_64_hemsworh_1.txt
    │   ├── base_64_hemsworh_2.png
    │   ├── base_64_hemsworh_2.txt
    │   ├── base_64_male_tpdne.png
    │   ├── base_64_male_tpdne.txt
    │   ├── base_64_rd.png
    │   └── base_64_rd.txt
    ├── poetry.lock
    └── pyproject.toml


"""
import base64
import sys
from io import BytesIO
from pathlib import Path

from PIL import Image


def decode_and_save_image(base64_filename: Path):
    """

    >>> base64_filename
        PosixPath('images/base_64_cca.txt')

    >>> base64_filename.stem
    'base_64_cca'

    >>> output_image = base64_filename.parent / (base64_filename.stem + ".png")
    >>> output_image
    PosixPath('images/base_64_cca.png')

    """
    output_image_filename = base64_filename.parent / (
        base64_filename.stem.removeprefix("base_64_") + ".png"
    )
    print(f"{base64_filename=}\n{output_image_filename=}")
    f = open(base64_filename, "r")
    data = f.read()
    f.close()
    im = Image.open(BytesIO(base64.b64decode(data)))
    im.save(output_image_filename, "PNG")


if __name__ == "__main__":
    """Point d'entrée du script Python."""
    base64_filename = Path(".")
    for i, argument in enumerate(sys.argv):
        if argument == "--file":
            base64_filename = Path(sys.argv[i + 1])

    decode_and_save_image(base64_filename)
