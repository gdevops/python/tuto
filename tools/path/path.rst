.. index::
   pair: Python ; Path
   pair: delete ; Path

.. _path_and_base64:

==================================
**Path and base64**
==================================



Delete files under a directory
==============================

.. code-block:: python
   :linenos:

    directory = Path("tmp/")
    for entry in repertoire.rglob("stats_employe_*.csv"):
        entry.unlink()


Building a zip file from csv files
=====================================

.. code-block:: python
   :linenos:

    directory = Path("tmp/")
    zipfilename_partiel = f"stats_employes_{selected_year}.zip"
    zipfilename = f"tmp/{zipfilename_partiel}"
    with zipfile.ZipFile(zipfilename, "w", zipfile.ZIP_DEFLATED) as zip_file:
        for entry in directory.rglob("stats_employe_*.csv"):
            zip_file.write(entry, entry.relative_to(dir))


decode base64
===============

.. literalinclude:: decode_base64/decode_base64.py
   :linenos:
