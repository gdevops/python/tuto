.. index::
   pair: Python ; pycln

.. _pycln_tool:

==========================================================================
**pycln** (A formatter for finding and removing unused import statements)
==========================================================================

.. seealso::

   - https://github.com/hadialqattan/pycln
   - https://x.com/hadi_alqattan
   - https://x.com/willmcgugan/status/1312681619363332102?s=20
   - https://github.com/hadialqattan/pycln/releases/tag/0.0.1-alpha.2




Announce
=========

.. seealso::

   - https://x.com/willmcgugan/status/1312681619363332102?s=20

Installation
==============

Pycln requires Python 3.6+ and can be easily installed using the most
common Python packaging tools. We recommend installing the latest
stable release from PyPI with pip:

::

    $ pip install pycln


Usage
======

The Simplest Usage

By default Pycln will remove any unused import statement, So the simplest
usage is to specify only the path:

    $ pycln [PATH]


CLI Arguments
==============

.. seealso::

   - https://github.com/hadialqattan/pycln/blob/master/pycln/cli.py


Specify a directory to handle all it's subdirs/files (recursively)::

    $ pycln my/project/directory

Specify a file::

    $ pycln my_python_file.py

Specify multiple directories and files::

    $ pycln dir1/ dir2/ main.py cli.py

Assume that we have three files (util\_a.py, util\_b.py, test_a.py) on a
directory called project and we want to reformat only files that start
with util\_::

    $ pycln /path_to/project/ --include util_.*  # or -i util_.*


Remove all unused import statements whether they has side effects or not!
Faster results, because Pycln will skip side effects analyzing::

    $ pycln /path/ --all  # or -a


Get configs only from a config file::

    $ pycln --config config_file.cfg  # .toml, .json, .yaml, .yml
