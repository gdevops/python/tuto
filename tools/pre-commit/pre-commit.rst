.. index::
   pair: Python ; pre-commit
   ! pre-commit

.. _pre_commit:

===========================================================================================
**pre-commit** (A framework for managing and maintaining multi-language pre-commit hooks)
===========================================================================================

- https://github.com/pre-commit/pre-commit
- https://pre-commit.com/
- https://pre-commit.com/hooks.html

.. toctree::
   :maxdepth: 3

   versions/versions
