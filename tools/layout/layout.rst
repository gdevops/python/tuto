.. index::
   pair: Python ; Packaging
   ! layout
   ! templates
   ! project templates

.. _python_layout:

==========================================================
Python layout, project templates (physical architecture)
==========================================================

.. seealso::

   - :ref:`physical_architecture`
   - https://discuss.python.org/c/packaging


.. toctree::
   :maxdepth: 2

   cookiecutter/cookiecutter


.. toctree::
   :maxdepth: 6

   create_python_package/create_python_package
   pyscaffold/pyscaffold
