
.. index::
   pair: Versions ; create-python-package

.. _python_create_python_package_versions:

==================================
create-python-package versions
==================================

.. seealso::

   - https://github.com/cs01/create-python-package/releases

.. toctree::
   :maxdepth: 3

   0.3.0/0.3.0
