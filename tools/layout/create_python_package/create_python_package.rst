
.. index::
   pair: Python ; create-python-package
   ! create-python-package

.. _python_create_python_package:

==================================================================================
**create-python-package** (Initialize a new Python package using best practices)
==================================================================================

.. seealso::

   - https://github.com/cs01/create-python-package
   - https://x.com/grassfedcode


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
