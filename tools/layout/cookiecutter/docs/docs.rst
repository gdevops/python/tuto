
.. index::
   pair: cookiecutter; docs

.. _cookiecutter_docs:

==================================
cookiecutter docs
==================================

.. seealso::

   - https://github.com/audreyr/cookiecutter/tree/master/docs
   - https://cookiecutter.readthedocs.io/en/latest/
