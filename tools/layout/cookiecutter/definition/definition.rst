
.. index::
   pair: Definition ; cookiecutter

.. _cookiecutter_def:

==================================
cookiecutter definition
==================================


github cookiecutter definition


.. seealso::

   - https://github.com/audreyr/cookiecutter/blob/master/README.rst


.. include:: README.rst
