
.. index::
   pair: Python ; cookiecutter
   ! cookiecutter

.. _python_cookiecutter:

==================================
cookiecutter
==================================

.. seealso::

   - :ref:`physical_architecture`
   - https://github.com/audreyr/cookiecutter
   - https://cookiecutter.readthedocs.io/en/latest/
   - https://github.com/audreyr/cookiecutter/tree/master/docs


.. figure:: logo_cookiecutter.png
   :align: center
   :width: 300

.. toctree::
   :maxdepth: 6

   definition/definition
   docs/docs
   django/django
   python/python
