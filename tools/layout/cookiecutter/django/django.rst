
.. index::
   pair: cookiecutter; Django

.. _python_cookiecutter_django:

==================================
cookiecutter for Django projects
==================================

.. seealso::

   - :ref:`physical_architecture`
   - :ref:`django_cookicutter`
