
.. index::
   pair: cookiecutter; Python

.. _python_cookiecutter_py:

==================================
cookiecutter for Python projects
==================================

.. seealso::

   - :ref:`physical_architecture`
   - https://github.com/audreyr/cookiecutter#python

.. toctree::
   :maxdepth: 3

   python_gitlab_cookie/python_gitlab_cookie
