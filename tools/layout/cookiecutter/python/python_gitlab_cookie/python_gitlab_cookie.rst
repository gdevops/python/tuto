
.. index::
   pair: cookiecutter; python-gitlab-cookie
   pair: gitlab ; python-gitlab-cookie

.. _python_gitlab_cookie:

==================================
python-gitlab-cookie
==================================

.. seealso::

   - :ref:`physical_architecture`
   - https://gitlab.yapbreak.fr/olivaa/python-gitlab-cookie
