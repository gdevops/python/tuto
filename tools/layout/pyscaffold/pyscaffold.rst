
.. index::
   pair: Python ; pyscaffold
   ! pyscaffold

.. _pyscaffold:

===============================================================================
**pyscaffold** (Template tool for putting up the scaffold of a Python project)
===============================================================================

.. seealso::

   - https://github.com/pyscaffold/pyscaffold
   - https://x.com/FlorianWilhelm
   - https://x.com/PyScaffold
   - https://pyscaffold.org/en/latest/


.. figure:: logo_pyscaffold.png
   :align: center
   :width: 300


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
