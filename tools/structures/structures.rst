.. index::
   pair: Python ; Structures

.. _python_structures:

==================================
Python structures
==================================


.. toctree::
   :maxdepth: 6

   namedtuple/namedtuple
