.. index::
   ! Afpy

.. _afpy:

=================================================================================================================
Association Francophone Python (Afpy)
=================================================================================================================

- https://www.afpy.org/
- https://discuss.afpy.org/
- https://x.com/asso_python_fr
- https://www.pycon.fr/
- https://docs.python.org/fr
- https://www.afpy.org/communaute
- https://www.afpy.org/discord

L’association
================

L’association « A.F.P.Y. » , Association Francophone Python, fondée
le 11 décembre 2004 sous le régime de la loi du 1er juillet 1901 a pour
but la vulgarisation auprès d’un public francophone du langage de
programmation python et de ses applications.


Introduction
===============

L'AFPy promeut le langage Python que ce soit auprès d'un public averti
ou débutant.

Les projets de l'AFPy :

- L'organisation annuelle de la `PyConFr <https://www.pycon.fr/2020/>`_.
- La traduction de la `documentation de Python en français <https://docs.python.org/fr>`_ (Licence CC0).
- La création d'un outil d'apprentissage et d'enseignement de Python : `HackInScience <https://www.hackinscience.org/>`_ (Licence MIT).
- L'organisation `d'évènements locaux ou en ligne <https://discuss.afpy.org/c/meetups/12>`_.


Charte
========

- https://www.afpy.org/docs/charte

Le but de l'Association Francophone Python (AFPy) est d'assurer la promotion
du langage de programmation Python auprès du plus large public possible.

Nous diffusons nos connaissances et sommes ouverts aux apports extérieurs
car la diversité, l'écoute réciproque, la solidarité et la démocratie
élargissent les horizons et accroissent le potentiel de l'AFPy et de
chacun de ses membres.
En conséquence, nous encourageons donc la participation aux divers évènements
que nous (co-)organisons de toutes les personnes de la communauté Python
ou souhaitant en faire partie.
Dans ce cadre, l'AFPy souhaite que toute personne intéressée soit accueillie
de manière agréable et enrichissante.

L'AFPy s'attend donc à ce que chaque membre de l'association fasse preuve
de respect et de courtoisie envers les autres membres de l'association
et les personnes rencontrées dans le cadre de tout évènement organisé
(formellement ou non) par l'association.

Ce texte est notamment inspiré par le code de conduite établi par
l'Ada Initiative et par la loi française de lutte contre les discriminations.

Communauté
===========

- https://www.afpy.org/communaute


Discord
=========

- https://www.afpy.org/discord

Twitter
=======

- https://x.com/asso_python_fr
