.. index::
   pair: Python; Community

.. _python_community:

=================================================================================================================
**Python community**
=================================================================================================================

- https://rstockm.github.io/mastowall/?hashtags=python&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=python&server=https://fosstodon.org

- https://fedidevs.com/python/
- https://fedidevs.com/starter-packs/?q=python 


.. toctree::
   :maxdepth: 3


   france/france
   psf/psf
