.. index::
   pair: pdm ; Packaging
   ! pdm

.. _pdm:

===============================================================================================
**pdm** A modern Python package and dependency manager supporting the latest PEP standards
===============================================================================================

- https://packaging.python.org/en/latest/key_projects/#pdm
- https://pdm-project.org/en/latest/
- https://www.pyopensci.org/python-package-guide/package-structure-code/python-package-build-tools.html#pdm
- https://github.com/frostming (Love 🐍 Python, 📷 Photography and beautiful things)
- https://mas.to/@frostming


.. toctree::
   :maxdepth: 3
   
   author/author  
   description/description 
   tips/tips
   versus/versus
   plugins/plugins
   versions/versions
