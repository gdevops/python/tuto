
.. _pdm_versions:

===============================================================================================
**pdm** versions
===============================================================================================

- https://pdm-project.org/latest/dev/changelog/
- https://github.com/pdm-project/pdm/releases


.. toctree::
   :maxdepth: 3
   
   2.16.0/2.16.0
