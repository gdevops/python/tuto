
.. _pdm_2_16_0:

===============================================================================================
**pdm** 2.16.0 (2024-06-26)
===============================================================================================

- https://github.com/pdm-project/pdm/releases/tag/2.16.0


Features & Improvements
==========================

- Add --no-extras to pdm export to strip extras from the requirements. 
  Now the default behavior is to keep extras. (#2519)
- Support `PEP 723: running scripts with inline metadata in standalone 
  environment with dependencies. (#2924) <https://github.com/pdm-project/pdm/pull/2924>`_
- pdm use and pdm python install now take requires-python into account (incl. from pyproject.toml) 
  if python version not specified and pdm use provides auto installation by that. (#2943)
- --no-isolation no longer installs build-requires nor dynamic build dependencies, to be consistent with pip. (#2944)
- Add notifiers in CLI output when global project is being used. (#2952)
- Use tool.pdm.resolution table when calculating the content hash of 
  project file, previously only overrides table was used.
  This will change the hash already stored in the lockfile, so bump the 
  lockfile version to 4.4.2. (#2956)

PEP 723
=========



Documentation
===============

- Clarify the purposes of pdm outdated and --unconstrained option. (#2965)
- Some clarifications on the interpreter selection and central package cache. (#2967)
