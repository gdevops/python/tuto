
.. _pdm_desc:

===============================================================================================
**pdm** description
===============================================================================================

- https://github.com/pdm-project/pdm#what-is-pdm

What is PDM ?
================

PDM is meant to be a next generation Python package management tool. 

It was originally built for personal use. 

If you feel you are going well with Pipenv or Poetry and don't want to 
introduce another package manager, just stick to it. 

But if you are missing something that is not present in those tools, you 
can probably find some goodness in pdm.


Highlights of features
============================

- Simple and fast dependency resolver, mainly for large binary distributions.
- A PEP 517 build backend.
- PEP 621 project metadata.
- Flexible and powerful plug-in system.
- Versatile user scripts.
- Install Pythons using indygreg's python-build-standalone.
- Opt-in centralized installation cache like pnpm.


On social networks in 2024
============================

- :ref:`python_2024:pdm_2024_07_06`
