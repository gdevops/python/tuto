
.. _pdm_versus_poetry:

===============================================================================================
**pdm** versus poetry
===============================================================================================


Poetry manages environments and dependencies in a similar way to Pipenv, 
but it can also build .whl files with your code, and it can upload wheels 
and source distributions to PyPI. 

It has a pretty user interface and users can customize it via a plugin. 

Poetry uses the pyproject.toml standard, but it does not follow the 
standard specifying how metadata should be represented in a pyproject.toml file 
(PEP 621), instead using a custom [tool.poetry] table. 

.. note:: This is partly because Poetry came out before PEP 621.

