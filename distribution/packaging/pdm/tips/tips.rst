
.. _pdm_tips:

===============================================================================================
**pdm** Tips
===============================================================================================

::

    # https://pdm-project.org/en/latest/usage/venv/#activate-a-virtualenv
    pdm() {
      local command=$1

      if [[ "$command" == "shell" ]]; then
          eval $(pdm venv activate)
      else
          command pdm $@
      fi
    }
