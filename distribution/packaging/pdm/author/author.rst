.. index::
   pair: pdm author; frostming

.. _frostming:

===============================================================================================
**pdm** author frostming
===============================================================================================

- https://github.com/frostming (Love 🐍 Python, 📷 Photography and beautiful things)
- https://mas.to/@frostming

