
.. _pdm_plugins:

===============================================================================================
**pdm** plugins
===============================================================================================

- https://pdm-project.org/latest/dev/write/
- https://github.com/pdm-project/awesome-pdm
- https://github.com/pdm-project/awesome-pdm?tab=readme-ov-file#plugins


Description
==============


PDM is aiming at being a community driven package manager. 

It is shipped with a full-featured plug-in system, with which you can:

- Develop a new command for PDM
- Add additional options to existing PDM commands
- Change PDM's behavior by reading additional config items
- Control the process of dependency resolution or installation

Awsome PDM plugins
=====================

- https://github.com/pdm-project/awesome-pdm?tab=readme-ov-file#plugins

.. toctree::
   :maxdepth: 3
   
   pdm-autoexport/pdm-autoexport  
   sync-pre-commit-lock/sync-pre-commit-lock 

