.. index::
   pair: pdm plugin; sync-pre-commit-lock

.. _sync_pre_commit_lock_plugin:

======================================================================================================================================================
**sync-pre-commit-lock** plugin Automatically sync your pre-commit hooks version from your PDM or Poetry lockfile, and install them automatically. 
======================================================================================================================================================

- https://github.com/GabDug/sync-pre-commit-lock

Features
============

- 🔁 Sync pre-commit versions with your lockfile
- ⏩ Run every time you run the lockfile is updated, not as a pre-commit hook
- 🔄 Install pre-commit hooks automatically, no need to run pre-commit install manually
- 💫 Preserve your pre-commit config file formatting
- 🍃 Lightweight, only depends on strictyaml

