.. index::
   pair: pdm plugin; pdm-autoexport

.. _pdm_autoexport_plugin:

===============================================================================================
**pdm-autoexport** plugin
===============================================================================================

- https://github.com/pdm-project/pdm-autoexport
- https://github.com/pdm-project/awesome-pdm?tab=readme-ov-file#plugins


Installation
======================

Install the plugin with PDM CLI::

    pdm plugin add pdm-autoexport


Or using `pipx inject`::

    pipx inject pdm pdm-autoexport

Usage
========

Configure the requirement mapping in `pyproject.toml`

.. code-block:: toml

    [[tool.pdm.autoexport]]
    filename = "requirements/prod.txt"
    groups = ["default"]

    [[tool.pdm.autoexport]]
    filename = "setup.py"
    format = "setuppy"
