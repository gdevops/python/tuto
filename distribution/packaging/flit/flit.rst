.. index::
   pair: flit ; Packaging
   ! flit

.. _flit:

==================================================================
**flit** 
==================================================================

- https://flit.pypa.io/en/latest/
- https://packaging.python.org/en/latest/key_projects/#flit
