.. index::
   pair: pipenv ; installation
   ! pipenv installation

.. _pipenv_installation:

=====================================================
pipenv installation
=====================================================

.. seealso::

   - https://github.com/pypa/pipenv
   - https://pipenv.pypa.io/en/latest/


Crude Installation of Pipenv
================================

If you don’t even have pip installed, you can use this crude installation
method, which will bootstrap your whole system:

::

    $ curl https://raw.githubusercontent.com/pypa/pipenv/master/get-pipenv.py | python



::

    curl https://raw.githubusercontent.com/pypa/pipenv/master/get-pipenv.py | python
      % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                     Dload  Upload   Total   Spent    Left  Speed
    100 1558k  100 1558k    0     0  2979k      0 --:--:-- --:--:-- --:--:-- 2973k
    Collecting pip
      Using cached https://files.pythonhosted.org/packages/54/0c/d01aa759fdc501a58f431eb594a17495f15b88da142ce14b5845662c13f3/pip-20.0.2-py2.py3-none-any.whl
    Collecting wheel
      Using cached https://files.pythonhosted.org/packages/8c/23/848298cccf8e40f5bbb59009b32848a4c38f4e7f3364297ab3c3e2e2cd14/wheel-0.34.2-py2.py3-none-any.whl
    Collecting pipenv
      Downloading https://files.pythonhosted.org/packages/13/b4/3ffa55f77161cff9a5220f162670f7c5eb00df52e00939e203f601b0f579/pipenv-2018.11.26-py3-none-any.whl (5.2MB)
        100% |████████████████████████████████| 5.2MB 323kB/s
    Collecting setuptools>=36.2.1 (from pipenv)
      Downloading https://files.pythonhosted.org/packages/a0/df/635cdb901ee4a8a42ec68e480c49f85f4c59e8816effbf57d9e6ee8b3588/setuptools-46.1.3-py3-none-any.whl (582kB)
        100% |████████████████████████████████| 583kB 1.1MB/s
    Collecting virtualenv-clone>=0.2.5 (from pipenv)
      Using cached https://files.pythonhosted.org/packages/83/b8/cd931487d250565392c39409117436d910232c8a3ac09ea2fb62a6c47bff/virtualenv_clone-0.5.4-py2.py3-none-any.whl
    Collecting virtualenv (from pipenv)
      Using cached https://files.pythonhosted.org/packages/ed/8e/017ae1fa91c225c27235a73e45e1e82b6a5de1fc7c99ffde68914ac78048/virtualenv-20.0.15-py2.py3-none-any.whl
    Collecting certifi (from pipenv)
      Using cached https://files.pythonhosted.org/packages/b9/63/df50cac98ea0d5b006c55a399c3bf1db9da7b5a24de7890bc9cfd5dd9e99/certifi-2019.11.28-py2.py3-none-any.whl
    Collecting distlib<1,>=0.3.0 (from virtualenv->pipenv)
      Downloading https://files.pythonhosted.org/packages/7d/29/694a3a4d7c0e1aef76092e9167fbe372e0f7da055f5dcf4e1313ec21d96a/distlib-0.3.0.zip (571kB)
        100% |████████████████████████████████| 573kB 1.4MB/s
    Collecting filelock<4,>=3.0.0 (from virtualenv->pipenv)
      Downloading https://files.pythonhosted.org/packages/93/83/71a2ee6158bb9f39a90c0dea1637f81d5eef866e188e1971a1b1ab01a35a/filelock-3.0.12-py3-none-any.whl
    Collecting appdirs<2,>=1.4.3 (from virtualenv->pipenv)
      Downloading https://files.pythonhosted.org/packages/56/eb/810e700ed1349edde4cbdc1b2a21e28cdf115f9faf263f6bbf8447c1abf3/appdirs-1.4.3-py2.py3-none-any.whl
    Collecting six<2,>=1.9.0 (from virtualenv->pipenv)
      Downloading https://files.pythonhosted.org/packages/65/eb/1f97cb97bfc2390a276969c6fae16075da282f5058082d4cb10c6c5c1dba/six-1.14.0-py2.py3-none-any.whl
    Installing collected packages: pip, wheel, setuptools, virtualenv-clone, distlib, filelock, appdirs, six, virtualenv, certifi, pipenv
      Found existing installation: pip 19.2.3
        Uninstalling pip-19.2.3:
          Successfully uninstalled pip-19.2.3
      Found existing installation: setuptools 41.2.0
        Uninstalling setuptools-41.2.0:
          Successfully uninstalled setuptools-41.2.0
      Running setup.py install for distlib ... done
    Successfully installed appdirs-1.4.3 certifi-2019.11.28 distlib-0.3.0 filelock-3.0.12 pip-20.0.2 pipenv-2018.11.26 setuptools-46.1.3 six-1.14.0 virtualenv-20.0.15 virtualenv-clone-0.5.4 wheel-0.34.2
