
.. _hatch_2023_08_01:

==================================================================
2023-08-01 **Switching to Hatch**
==================================================================

- https://andrich.me/2023/08/switching-to-hatch/


Hatch – a new love ?
========================

A few weeks ago, I decided to look a bit more into Hatch. 

Numerous people are using and telling good things about it on Mastodon. 

Some prominent projects I enjoy using, are using Hatch. 

**It seems to be more standard compliant than Poetry concerning the project 
metadata and dependency management**. 

And finally, people are saying it is faster than the other tools. 

All of these are interesting features that have triggered the tooling 
nerd in me.

I did some experiments with it and then decided to convert my project 
django-tailwind-cli to use Hatch for project management. 

And I also built a Django template to use Hatch to manage it too.
