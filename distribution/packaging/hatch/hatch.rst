.. index::
   pair: hatch ; Packaging
   ! hatch

.. _hatch:

==================================================================
**hatch** 
==================================================================

- https://hatch.pypa.io/latest/why/
- https://packaging.python.org/en/latest/key_projects/#hatch


.. toctree::
   :maxdepth: 3
   
   2023/2023
