.. index::
   pair: rye ; Packaging
   ! rye

.. _rye:

==================================================================
**rye** a Hassle-Free Python Experience
==================================================================

- https://github.com/astral-sh/rye
- https://rye.astral.sh/guide/

.. toctree::
   :maxdepth: 3
   
   description/description
   versions/versions
   
   
