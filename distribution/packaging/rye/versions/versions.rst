.. index::
   pair: rye ; versions

.. _rye_versions:

==================================================================
**rye** versions
==================================================================

- https://github.com/astral-sh/rye/releases.atom

.. toctree::
   :maxdepth: 3
   
   0.36.0/0.36.0
