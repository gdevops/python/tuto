.. index::
   pair: rye ; description

.. _rye_desc:

==================================================================
**rye** description |rye|
==================================================================

Description  |rye|
======================

`Rye https://github.com/astral-sh/rye <https://github.com/astral-sh/rye>`_ 
is a comprehensive project and package management solution for Python. 

Born from `its creator's <https://github.com/mitsuhiko>`_ desire to establish 
a one-stop-shop for all  Python users, Rye provides a unified experience 
to install and manages: 

- Python installations, 
- pyproject.toml based projects, 
- dependencies and virtualenvs seamlessly. 

It's designed to accommodate complex projects, monorepos and to facilitate 
global tool installations. 

Curious ? `Watch an introduction <https://youtu.be/q99TYA7LnuA>`_.

A hassle-free experience for Python developers at every level.


In The Box
=================

- https://github.com/indygreg/python-build-standalone/
- https://github.com/astral-sh/ruff

Rye picks and ships the right tools so you can get started in minutes:

- **Bootstraps Python**: it provides an automated way to get access to the 
  `amazing Indygreg Python Builds <https://github.com/indygreg/python-build-standalone/>`_ as well as the PyPy binary distributions.
- Linting and Formatting: it bundles `ruff <https://github.com/astral-sh/ruff>`_ and makes it available with rye lint and rye fmt.
- Managing Virtualenvs: it uses the well established virtualenv library under the hood.
- Building Wheels: it delegates that work largely to `build https://pypi.org/project/build/ <https://pypi.org/project/build/>`_
- Publishing: its publish command uses `twine https://pypi.org/project/twine/ <https://pypi.org/project/twine/>`_ to accomplish this task.
- **Locking and Dependency Installation**: is today implemented by using `uv https://github.com/astral-sh/uv <https://github.com/astral-sh/uv>`_ 
  with a fallback to `unearth <https://pypi.org/project/unearth/>`_ and `pip-tools <https://github.com/jazzband/pip-tools/>`_.
- Workspace support: Rye lets you work with complex projects consisting of multiple libraries.


On social networks in 2024
============================

- :ref:`python_2024:rye_2024_07_06`
