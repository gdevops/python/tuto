
.. _uv_versions:

=====================================================================================
**uv** versions
=====================================================================================

- https://github.com/astral-sh/uv/releases
- https://github.com/astral-sh/uv/releases.atom


.. toctree::
   :maxdepth: 3
   
   0.2.24/0.2.24
