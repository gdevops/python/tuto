
.. _uv_desc:

=====================================================================================
**uv** description
=====================================================================================

- https://github.com/astral-sh/uv

Description
===============

An extremely fast Python package installer and resolver, written in Rust. 
Designed as a drop-in replacement for common pip and pip-tools workflows.


Highlights
================

- ⚖️ Drop-in replacement for common pip, pip-tools, and virtualenv commands.
- ⚡️ 10-100x faster than pip and pip-tools (pip-compile and pip-sync).
- 💾 Disk-space efficient, with a global cache for dependency deduplication.
- 🐍 Installable via curl, pip, pipx, etc. uv is a static binary that can 
  be installed without Rust or Python.
- 🧪 Tested at-scale against the top 10,000 PyPI packages.
- 🖥️ Support for macOS, Linux, and Windows.
- 🧰 Advanced features such as dependency version overrides and alternative 
  resolution strategies.
- ⁉️ Best-in-class error messages with a conflict-tracking resolver.
- 🤝 Support for a wide range of advanced pip features, including editable 
  installs, Git dependencies, direct URL dependencies, local dependencies, 
  constraints, source distributions, HTML and JSON indexes, and more.

uv is backed by Astral, the creators of Ruff.


Liens 2024
============

- :ref:`python_2024:uv_2024_07_06`
