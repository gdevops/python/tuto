.. index::
   pair: uv ; An extremely fast Python package installer and resolver, written in Rust
   ! uv

.. _uv:

=====================================================================================
**uv** An extremely fast Python package installer and resolver, written in Rust
=====================================================================================

- https://github.com/astral-sh/uv
- https://x.com/astral_sh
- https://nitter.poast.org/astral_sh/rss

.. toctree::
   :maxdepth: 3
   
   description/description
   versions/versions
   
   
