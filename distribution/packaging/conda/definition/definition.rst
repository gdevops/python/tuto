

.. _conda_def:

==================================
Conda definitions
==================================



github conda definition
==========================

.. seealso::

   - https://github.com/conda/conda/blob/master/README.rst


Conda is a cross-platform, **language-agnostic binary package manager**.

It is the package manager used by Anaconda installations, but it may be used
for other systems as well.
Conda makes environments first-class citizens, making it easy to create
independent environments even for C libraries.
Conda is written entirely in Python, and is BSD licensed open source.

Conda is enhanced by organizations, tools, and repositories created and
managed by the amazing members of the conda community.
Some of them can be found here_.


.. _here: https://github.com/conda/conda/wiki/Conda-Community

conda.io definition
=====================


.. seealso::

   - https://conda.io/projects/conda/en/latest/

Package, dependency and environment management for any language:

- Python, R, Ruby, Lua, Scala, Java, JavaScript, C/ C++, FORTRAN

Conda is an open source package management system and environment management
system that runs on Windows, macOS and Linux.

Conda quickly installs, runs and updates packages and their dependencies.

Conda easily creates, saves, loads and switches between environments on your
local computer.
It was created for Python programs, but it can package and distribute software
for any language.

Conda as a package manager helps you find and install packages.

If you need a package that requires a different version of Python, you do not
need to switch to a different environment manager, because conda is also an
environment manager.
With just a few commands, you can set up a totally separate environment to
run that different version of Python, while continuing to run your usual
version of Python in your normal environment.

In its default configuration, conda can install and manage the thousand
packages at repo.continuum.io that are built, reviewed and maintained
by Anaconda®.

Conda can be combined with continuous integration systems such as Travis CI
and AppVeyor to provide frequent, automated testing of your code.

The conda package and environment manager is included in all versions of
Anaconda®, Miniconda and Anaconda Repository.
Conda is also included in Anaconda Enterprise , which provides on-site
enterprise package and environment management for Python, R, Node.js, Java
and other application stacks.

Conda is also available on PyPI, although that approach may not be as up
to date.
