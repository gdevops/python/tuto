

.. _miniconda_def:

==================================
miniconda definition
==================================

.. seealso::

   - https://conda.io/en/latest/
   - https://github.com/conda/conda

A free minimal installer for conda. Miniconda is a small, bootstrap version of
Anaconda that includes only conda, Python, the packages they depend on and a
small number of other useful packages, including pip, zlib and a few others.

Use the conda install command to install 720+ additional conda packages from the Anaconda repository.

Because Miniconda is a Python distribution, and it can make installing Python quick and easy even for new users.
