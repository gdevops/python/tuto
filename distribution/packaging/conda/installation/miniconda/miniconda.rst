
.. index::
   ! miniconda

.. _minconda:

==================================
miniconda
==================================

.. seealso::

   - https://conda.io/en/latest/miniconda.html

.. toctree::
   :maxdepth: 3

   definition/definition
