
.. index::
   pair: Conda ; Installation

.. _conda_install:

==================================
Conda installation
==================================

.. seealso::

   - https://conda.io/projects/conda/en/latest/user-guide/install/index.html



Installation
=============

The fastest way to obtain conda is to install Miniconda, a mini version of
Anaconda that includes only conda and its dependencies.

If you prefer to have conda plus over 720 open source packages, install Anaconda.

We recommend you install Anaconda for the local user, which does not require
administrator permissions and is the most robust type of installation.

You can also install Anaconda system wide, which does require administrator
permissions.


Miniconda
==========


.. toctree::
   :maxdepth: 6

   miniconda/miniconda
