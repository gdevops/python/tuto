.. index::
   pair: Conda ; Versions

.. _conda_versions:

==================================
Conda versions
==================================

.. seealso::

   - https://github.com/conda/conda/releases


.. toctree::
   :maxdepth: 6

   4.6.14/4.6.14
