.. index::
   pair: Conda ; Tutorials


.. _conda_tuto_2019_04_18:

==================================================================
Packaging Python inside your organization with GitLab and Conda
==================================================================

.. seealso::

   - https://stefan.sofa-rockers.org/2019/04/18/python-packaging-gitlab-conda/
   - https://gitlab.com/ownconda
