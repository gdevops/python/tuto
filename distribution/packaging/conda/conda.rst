.. index::
   pair: Conda ; Packaging
   ! Conda

.. _conda:

==================================
Conda
==================================

.. seealso::

   - https://conda.io/en/latest/
   - https://github.com/conda/conda

.. figure:: conda_logo.svg
   :align: center
   :width: 300

.. toctree::
   :maxdepth: 6

   definition/definition
   installation/installation
   tutorials/tutorials
   versions/versions
