.. index::
   pair: pip ; Packaging
   ! pip

.. _pip:

=====================================================
pip
=====================================================

.. seealso::

   - https://github.com/pypa/pip
