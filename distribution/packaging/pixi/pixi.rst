.. index::
   pair: pixi ; a cross-platform, multi-language package manager and workflow tool built on the foundation of the conda ecosystem
   ! pixi

.. _pixi:

===================================================================================================================================
**pixi** a cross-platform, multi-language package manager and workflow tool built on the foundation of the conda ecosystem |pixi|
===================================================================================================================================

- https://github.com/prefix-dev/pixi
- https://github.com/prefix-dev/pixi/releases.atom

.. toctree::
   :maxdepth: 3
   
   description/description
   versions/versions
   
   
