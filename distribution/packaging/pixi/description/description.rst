.. index::
   pair: pixi ; description
   ! pixi

.. _pixi_desc:

====================================================================================================================================
**pixi** provides developers exceptional experience similar to popular package managers like cargo or yarn, but for any language
====================================================================================================================================

- https://github.com/prefix-dev/pixi#pixi-package-management-made-easy
- https://prefix.dev/
- https://twitter.com/prefix_dev
- https://nitter.poast.org/prefix_dev/rss


Description |pixi|
====================

**pixi** is a cross-platform, multi-language package manager and workflow tool 
built on the foundation of the conda ecosystem. 

It provides developers with an **exceptional experience similar to popular 
package managers like cargo or yarn, but for any language**

Developed with ❤️ at `prefix.dev <https://prefix.dev/>`_

pixi supports:

- Python, 
- R, 
- C/C++, 
- Rust, 
- Ruby, 
- and many other languages.


Highlights
=================

- Supports multiple languages including Python, C++, and R using Conda packages. 
  You can find available packages on prefix.dev.
- Compatible with all major operating systems: Linux, Windows, macOS (including Apple Silicon).
- Always includes an up-to-date lock file.
- Provides a clean and simple Cargo-like command-line interface.
- Allows you to install tools per-project or system-wide.
- Entirely written in Rust and built on top of the rattler library.
