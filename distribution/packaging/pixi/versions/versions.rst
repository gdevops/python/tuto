.. index::
   pair: pixi ; Package management made easy 
   ! pixi

.. _pixi_versions:

========================================================================
**pixi** versions
========================================================================

- https://github.com/prefix-dev/pixi/releases
- https://github.com/prefix-dev/pixi/releases.atom

.. toctree::
   :maxdepth: 3
   
   0.25.0/0.25.0
   
   
