.. index::
   pair: Python ; Packaging

.. _python_packaging:

==================================
Python packaging
==================================

- https://peps.python.org/topic/packaging/
- https://packaging.python.org/en/latest/key_projects/
- https://www.stuartellis.name/articles/python-modern-practices/ (2024-07-07)
- https://discuss.python.org/c/packaging
- https://cjolowicz.github.io/posts/hypermodern-python-01-setup/
- https://www.pyopensci.org/python-package-guide/package-structure-code/intro.html

.. toctree::
   :maxdepth: 6

   peps/peps
   news/news
   conda/conda
   flit/flit
   hatch/hatch
   pdm/pdm
   pip/pip
   pixi/pixi
   pipenv/pipenv
   poetry/poetry
   rye/rye
   uv/uv
