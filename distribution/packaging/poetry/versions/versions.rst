.. index::
   pair: poetry ; versions

.. _poetry_versions:

=====================================================
poetry versions
=====================================================

.. seealso::

   - https://python-poetry.org/history/
   - https://github.com/python-poetry/poetry/releases

.. toctree::
   :maxdepth: 3

   1.0.0/1.0.0
