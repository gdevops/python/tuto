.. index::
   pair: poetry ; 1.0.0 (2019-12-12)
   ! poetry

.. _poetry_1_0_0:

=====================================================
poetry 1.0.0 (2019-12-12)
=====================================================

.. seealso::

   - https://github.com/python-poetry/poetry/tree/1.0.0
   - https://github.com/python-poetry/poetry/releases/tag/1.0.0
   - https://python-poetry.org/blog/announcing-poetry-1-0-0.html
