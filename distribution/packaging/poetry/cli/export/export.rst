.. index::
   pair: poetry ; export

.. _poetry_export:

=====================================================
poetry **export**
=====================================================


.. seealso::

   - https://python-poetry.org/docs/cli/#export




Description
===========

This command exports the lock file to other formats.

::

    poetry export -f requirements.txt > requirements.txt


Only the **'requirements.txt' format** is currently supported.

Options
=========

::

    --format (-f): The format to export to. Currently, only 'requirements.txt' is supported.
    --output (-o): The name of the output file. If omitted, print to standard output.
    --dev: Include development dependencies.
    --extras (-E): Extra sets of dependencies to include.
    --without-hashes: Exclude hashes from the exported file.
    --with-credentials: Include credentials for extra indices.



::

    poetry export -f requirements.txt --without-hashes  > requirements.txt
