.. index::
   pair: poetry ; env

.. _poetry_env:

=====================================================
poetry **env**
=====================================================


.. seealso::

   - https://python-poetry.org/docs/cli/#env
   - https://python-poetry.org/docs/managing-environments/




Description
===========

The env command regroups sub commands to interact with the virtualenvs associated
with a specific project.


poetry help env
================

::

    poetry help env

::

    $ poetry help env
    USAGE
          poetry env
      or: poetry env info [-p]
      or: poetry env list [--full-path]
      or: poetry env remove <python>
      or: poetry env use <python>

    COMMANDS
      info
        Displays information about the current environment.

        -p (--path)          Only display the environment's path.

      list
        Lists all virtualenvs associated with the current project.

        --full-path          Output the full paths of the virtualenvs.

      remove
        Removes a specific virtualenv associated with the project.

        <python>             The python executable to remove the virtualenv for.

      use
        Activates or creates a new virtualenv for the current project.

        <python>             The python executable to use.

    GLOBAL OPTIONS
      -h (--help)            Display this help message
      -q (--quiet)           Do not output any message
      -v (--verbose)         Increase the verbosity of messages: "-v" for normal output, "-vv" for more verbose output and "-vvv" for debug
      -V (--version)         Display this application version
      --ansi                 Force ANSI output
      --no-ansi              Disable ANSI output
      -n (--no-interaction)  Do not ask any interactive question



poetry env info
========================

::

    poetry env info

::

    Virtualenv
    Python:         3.8.2
    Implementation: CPython
    Path:           /home/pvergain/.cache/pypoetry/virtualenvs/tuto-python-CXb74PZ_-py3.8
    Valid:          True

    System
    Platform: linux
    OS:       posix
    Python:   /home/pvergain/.pyenv/versions/3.8.2
