.. index::
   pair: poetry ; Packaging
   ! poetry

.. _poetry:

==================================================================
**poetry** Python dependency management and packaging made easy
==================================================================

- https://github.com/python-poetry/poetry
- https://python-poetry.org/
- https://python-poetry.org/
- https://x.com/SDisPater
- https://cjolowicz.github.io/posts/hypermodern-python-01-setup/


.. toctree::
   :maxdepth: 3

   cli/cli
   installation/installation
   versions/versions
