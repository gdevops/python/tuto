.. index::
   pair: poetry ; installation

.. _poetry_installation:

=====================================================
**pyenv and poetry installation on Debian**
=====================================================

- https://devguide.python.org/getting-started/setup-building/index.html#install-dependencies
- https://packaging.python.org/en/latest/guides/tool-recommendations/
- https://realpython.com/intro-to-pyenv/#installing-pyenv
- http://codingadventures.org/2020/08/30/how-to-install-pyenv-in-ubuntu/
- https://www.carlpearson.net/post/20210822-debian-pyenv-poetry/
- :ref:`pyenv_installation`

Introduction
===============

- https://www.carlpearson.net/post/20210822-debian-pyenv-poetry/ 🙏

Setting up a python development environment is annoying.
We’d especially like to avoid:

- Using the system python: it may be way out of date, have special
  modifications, or be required for the system to function (and therefore
  we should avoid messing with it).
- Installing packages globally: different projects may require different
  versions, and the system may have its own package requirements that
  conflict with ours.

The solution has two components:

- install and use whatever version of python you like
- create isolated environments using your preferred version of python for each project

Dependencies
==============

- https://devguide.python.org/getting-started/setup-building/index.html#install-dependencies

Debian/Ubuntu
----------------

::

    sudo apt-get install build-essential gdb lcov pkg-config \
      libbz2-dev libffi-dev libgdbm-dev libgdbm-compat-dev liblzma-dev \
      libncurses5-dev libreadline6-dev libsqlite3-dev libssl-dev \
      lzma lzma-dev tk-dev uuid-dev zlib1g-dev


python3.X-dev
-----------------

python3.X-dev  in order to compile with the Python.h.

::

    sudo apt-get install python-3.X-dev


GNU/Linux mint
================

::

    curl  https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python


::

    % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                     Dload  Upload   Total   Spent    Left  Speed
    100 27386  100 27386    0     0   1754      0  0:00:15  0:00:15 --:--:--  6708
    Retrieving Poetry metadata

    # Welcome to Poetry!

    This will download and install the latest version of Poetry,
    a dependency and package manager for Python.

    It will add the `poetry` command to Poetry's bin directory, located at:

    $HOME/.poetry/bin

    This path will then be added to your `PATH` environment variable by
    modifying the profile files located at:

    $HOME/.profile
    $HOME/.zprofile
    $HOME/.bash_profile

    You can uninstall at any time by executing this script with the --uninstall option,
    and these changes will be reverted.

    Installing version: 1.0.5
      - Downloading poetry-1.0.5-linux.tar.gz (23.37MB)

    Poetry (1.0.5) is installed now. Great!

    To get started you need Poetry's bin directory ($HOME/.poetry/bin) in your `PATH`
    environment variable. Next time you log in this will be done
    automatically.

    To configure your current shell run `source $HOME/.poetry/env`


.zshrc
========

::

    export PATH=$HOME/.poetry/bin:$PATH


source $HOME/.poetry/env
========================


poetry
========

::
    poetry

::

    Poetry version 1.0.5

    USAGE
      poetry [-h] [-q] [-v [<...>]] [-V] [--ansi] [--no-ansi] [-n] <command> [<arg1>] ... [<argN>]

    ARGUMENTS
      <command>              The command to execute
      <arg>                  The arguments of the command

    GLOBAL OPTIONS
      -h (--help)            Display this help message
      -q (--quiet)           Do not output any message
      -v (--verbose)         Increase the verbosity of messages: "-v" for normal output, "-vv" for more verbose output and "-vvv" for debug
      -V (--version)         Display this application version
      --ansi                 Force ANSI output
      --no-ansi              Disable ANSI output
      -n (--no-interaction)  Do not ask any interactive question

    AVAILABLE COMMANDS
      about                  Shows information about Poetry.
      add                    Adds a new dependency to pyproject.toml.
      build                  Builds a package, as a tarball and a wheel by default.
      cache                  Interact with Poetry's cache
      check                  Checks the validity of the pyproject.toml file.
      config                 Manages configuration settings.
      debug                  Debug various elements of Poetry.
      env                    Interact with Poetry's project environments.
      export                 Exports the lock file to alternative formats.
      help                   Display the manual of a command
      init                   Creates a basic pyproject.toml file in the current directory.
      install                Installs the project dependencies.
      lock                   Locks the project dependencies.
      new                    Creates a new Python project at <path>.
      publish                Publishes a package to a remote repository.
      remove                 Removes a package from the project dependencies.
      run                    Runs a command in the appropriate environment.
      search                 Searches for packages on remote repositories.
      self                   Interact with Poetry directly.
      shell                  Spawns a shell within the virtual environment.
      show                   Shows information about packages.
      update                 Update the dependencies as according to the pyproject.toml file.
      version                Shows the version of the project or bumps it when a valid bump rule is provided.
