.. index::
   pair: Python Packaging PEP ; PEP 723 (Inline script metadata, 2023)

.. _pep_723:

=========================================
**PEP 723** Inline script metadata (2023)
=========================================

- https://peps.python.org/pep-0723/
- https://discuss.python.org/t/40418/82   
