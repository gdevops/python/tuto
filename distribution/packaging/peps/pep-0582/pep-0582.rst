.. index::
   pair: Python Packaging PEP ; PEP 582 (Python local packages directory, 2019)

.. _pep_582:

======================================================
**PEP 582** Python local packages directory (2019)
======================================================

- https://peps.python.org/pep-0582/
- https://discuss.python.org/t/pep-582-python-local-packages-directory/963/430   
