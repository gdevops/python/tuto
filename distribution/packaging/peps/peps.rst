.. index::
   pair: Python Packaging; PEPs

.. _python_packaging_peps:

==================================
**Python packaging PEPs**
==================================

- https://peps.python.org/topic/
- https://peps.python.org/topic/packaging/


.. toctree::
   :maxdepth: 3
   
   pep-0723/pep-0723
   pep-0621/pep-0621
   pep-0582/pep-0582
   pep-0508/pep-0508
   
   
