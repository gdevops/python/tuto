.. index::
   pair: Python ; multi-staging
   ! multi-staging

.. _python_docker_multi:

==================================
Python docker multi-staging
==================================

.. seealso::

   :ref:`images_python`

.. toctree::
   :maxdepth: 3


   example_3/example_3
   example_2/example_2
   example_1/example_1
