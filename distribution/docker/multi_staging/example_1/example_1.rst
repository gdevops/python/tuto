
.. index::
   pair: multi-staging; Docker-first Python development

.. _python_docker_2019_04_12:

===============================================
Docker-first Python development (2019-04-12)
===============================================

.. seealso::

   - https://dev.to/jeremywmoore/docker-first-python-development-987
   - https://x.com/jeremywmoore


...

Except there's one thing still bothering me: changes to the service code
trigger a reinstallation of our test dependencies.

Yuck! Let's take another whack at our Dockerfile


.. code-block:: dockerfile
   :linenos:

    FROM python3 as service_deps
    COPY requirements.txt ./
    RUN pip install -r requirements.txt

    FROM deps as test_deps
    COPY test_requirements.txt ./
    RUN pip install -r test_requirements.txt

    FROM deps as builder
    COPY src ./src

    FROM test_deps as tests_builder
    COPY src ./src
    COPY tests ./tests

    FROM tests_builder as localdev
    ENTRYPOINT ["pytest"]
    CMD ["tests"]

    FROM tests_builder as tests
    RUN pytest tests

    FROM builder as service
    COPY docker-entrypoint.sh ./
    ENTRYPOINT ["docker-entrypoint.sh"]
    EXPOSE 3000


Ok that seems pretty complicated, here's a graph of our image topology::

     service_deps
        |   \
        |    -\
        |      \
        |       -test_deps
        |            |
        |            |
     builder     tests_builder
        |            |  -\
        |            |    -\
        |        localdev   -\
        |                     --tests
     service

I don't love that the builder and tests_builder stages both copy over the
source directory, but the real question is, does this still meet our initial
goals while avoiding excessive re-installs of test dependencies ?

Yeah, it seems to work pretty well.

Thanks to Docker's layer caching, we rarely have to re-install dependencies.
