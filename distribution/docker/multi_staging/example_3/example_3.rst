
.. index::
   pair: multi-staging; Example 3

.. _python_docker_multi_example_3:

=======================================================================
multi-staging example 3: Python 3.7 + LDAP + MariaDB + WSGI + Apache2
=======================================================================


.. literalinclude:: Dockerfile
   :linenos:
   :language: docker
