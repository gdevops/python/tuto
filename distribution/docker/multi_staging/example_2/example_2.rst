
.. index::
   pair: multi-staging; Example 2

.. _python_docker_multi_example_2:

====================================================
multi-staging example 2: Python 3.7 + LDAP + MariaD
====================================================


.. literalinclude:: Dockerfile_dev
   :linenos:
   :language: docker
