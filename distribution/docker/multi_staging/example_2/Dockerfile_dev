# dockerfile_dev
# Development environment (LDAP, MariaDB, no apache server)
#
# 2 stages:
#
# - first stage
#       building the python virtualenv with ldap support
#       (we need gcc and libldap2-dev libsasl2-dev libssl-dev)
# - second stage
#       - copying the python virtualenv environment under /opt/update_id3/docker
#       - install the ldap libraries
#       - copying the django project under /opt/update_id3/update_id3
#
##### Start stage 1 : building the Python virtual environment ##########
# https://hub.docker.com/_/python
FROM python:3.7.3-slim-stretch as builder

# gcc because we need regex and pyldap
# libldap2-dev because we need pyldap
RUN apt-get update \
    && apt-get install -y gcc libldap2-dev libsasl2-dev libssl-dev default-libmysqlclient-dev

# we need the bash shell for the 'source' binary
SHELL ["/bin/bash", "-c"]
WORKDIR /opt/update_id3/docker/
COPY ./update_id3/requirements_dev.txt .
RUN python -m venv .venv && \
    source .venv/bin/activate && \
    pip install -U pip wheel && \
    pip install -r requirements_dev.txt

#### Start stage 2 #########################################
#
#  volumes:
#     - ./docker-django/update_id3/:/opt/update_id3/update_id3 # permet d'accéder au code du host
FROM python:3.7.3-slim-stretch as deployer

# ajout d'informations à l'image pour pouvoir l'identifier plus facilement
LABEL maintainer "patrick.vergain@id3.eu"
LABEL description "Application update.id3.eu 3.0.0 (NOT RELEASED)"

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /opt/update_id3/docker/
COPY --from=builder /opt/update_id3/docker/.venv ./.venv

# we only need  the ldap and mariadb libraries
# https://packages.debian.org/fr/jessie/libldap-2.4-2
RUN apt-get update \
    && apt-get install -y tree libldap-2.4-2 default-libmysqlclient-dev \
    && apt-get clean


# copying the django project
COPY ./update_id3 /opt/update_id3/update_id3
WORKDIR /opt/update_id3/update_id3

    RUN mkdir -p /opt/update_id3/logs \
        && touch /opt/update_id3/logs/update_id3.log
