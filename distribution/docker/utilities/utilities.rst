.. index::
   pair: Docker ; utilities

.. _python_docker_utilities:

==================================
Python docker utilities
==================================



Delete the dangling images
===========================

::

    docker rmi $(docker images --filter "dangling=true" -q)
