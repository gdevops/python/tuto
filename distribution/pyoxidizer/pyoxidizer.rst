.. index::
   pair: pyoxidizer ; Distribution

.. _pyoxidizer:

===========================================================================
**pyoxidizer** a modern Python application packaging and distribution tool
===========================================================================

.. seealso::

   - https://github.com/indygreg/PyOxidizer
   - https://x.com/indygreg
   - https://pyoxidizer.readthedocs.io/en/latest/index.html

.. toctree::
   :maxdepth: 3

   news/news
   definition/definition
   versions/versions
