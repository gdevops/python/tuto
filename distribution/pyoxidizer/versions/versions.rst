.. index::
   pair: pyoxidizer ; Versions

.. _pyoxidizer_versions:

==================================
pyoxidizer versions
==================================


.. seealso::

   - https://github.com/indygreg/PyOxidizer/releases


.. toctree::
   :maxdepth: 3

   0.1.2/0.1.2
