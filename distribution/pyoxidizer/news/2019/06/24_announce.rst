
.. _pyoxidizer_news_2019_06_24:

======================================================================================
June 24, 2019 at 09:00 AM Building Standalone Python Applications with **PyOxidizer**
======================================================================================

.. seealso::

   - https://gregoryszorc.com/blog/2019/06/24/building-standalone-python-applications-with-pyoxidizer/
   - https://x.com/indygreg/status/1143187250743668736
   - https://www.zdnet.com/google-amp/article/programming-language-pythons-existential-threat-is-app-distribution-is-this-the-answer/




Introduction
=============

.. figure:: 24_announce/twitter_announce.png
   :align: center

   https://x.com/indygreg/status/1143187250743668736


Python application distribution is generally considered an unsolved problem.

At their `PyCon 2019 keynote talk`_, Russel Keith-Magee identified code distribution
as a potential black swan - an existential threat for longevity - for Python.

In their words, Python hasn't ever had a consistent story for how I give my
code to someone else, especially if that someone else isn't a developer and
just wants to use my application.

I completely agree.

And I want to add my opinion that unless your target user is a Python developer,
they shouldn't need to know anything about Python packaging, Python itself,
or even the existence of Python in order to use your application.
(And you can replace Python in the previous sentence with any programming
language or software technology: most end-users don't care about the
technical implementation, they just want to get stuff done.)

Today, I'm excited to announce the first release of PyOxidizer (project,
documentation), an open source utility that aims to solve the Python application
distribution problem! (The installation instructions are in the docs.)


.. _`PyCon 2019 keynote talk`:  https://www.youtube.com/watch?v=ftP5BQh1-YM

Standalone Single File, No Dependencies Executable Python Applications
=========================================================================

PyOxidizer's marquee feature is that it can produce a single file executable
containing a fully-featured Python interpreter, its extensions, standard
library, and your application's modules and resources.

In other words, you can have a single .exe providing your application.
And unlike other tools in this space which tend to be operating system specific,
PyOxidizer works across platforms (currently Windows, macOS, and Linux - the
most popular platforms for Python today). Executables built with PyOxidizer
have minimal dependencies on the host environment nor do they do anything
complicated at run-time.

I believe PyOxidizer is the only open source tool to have all these attributes.


On twitter
=============

Andrew godwin message
------------------------

.. figure:: 24_announce/andrew_godwin.png
   :align: center

   https://x.com/andrewgodwin/status/1143982061524418560


Sam et max
--------------

.. figure:: 24_announce/sam_et_max.png
   :align: center

   https://x.com/sam_et_max/status/1143714599772000256
