.. index::
   ! Simon Willison

.. _simon_willison:

=================================================================================================================
Simon Willison
=================================================================================================================

- https://github.com/simonw
- https://github.com/simonw.atom
- https://fedi.simonwillison.net/@simon
- https://fedi.simonwillison.net/@simon.rss
