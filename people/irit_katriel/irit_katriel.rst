.. index::
   ! Irit Katriel

.. _irit_katriel:

=================================================================================================================
Irit Katriel
=================================================================================================================


- https://x.com/IritKatriel
- :ref:`exception_groups`


CPython Core Dev. Working @Microsoft. Tweets don't always reflect my
own views, let alone anyone else's.
