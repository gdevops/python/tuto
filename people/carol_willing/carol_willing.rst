.. index::
   ! Carol Willing

.. _carol_willing:

=================================================================================================================
Carol Willing
=================================================================================================================

- https://x.com/WillingCarol
- willingconsulting.com
- https://github.com/willingc
- https://speakerdeck.com/willingc


Resume
========

Carol Willing (@WillingCarol), a leader in open source governance, is the
VP Learning @noteable_io and a three-term steering council member for
the Python language. She's also a Python core developer and Fellow.

Carol develops tools in the Jupyter notebooks ecosystem and is a
Project Jupyter Steering Council member.

She also serves on the Open Science Advisory Board for CZI and as an
Advisory Board member of @QuansightAI Labs.
