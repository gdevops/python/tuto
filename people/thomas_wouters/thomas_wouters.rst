.. index::
   pair: Thomas; Wouters
   ! Thomas Wouters

.. _thomas_wouters:

=================================================================================================================
**Thomas Wouters, Python 3.2 release manager**
=================================================================================================================

- https://x.com/Yhg1s (T. Wouters @Yhg1s Cat owner, Python developer, Googler, Python Steering Council and Python Software Foundation board member. (He/him or they/them.) A rejoint Twitter en juin 2011)
- https://discuss.python.org/u/thomas/summary
- https://discuss.python.org/t/steering-council-nomination-thomas-wouters-2022-term/11946


.. figure:: images/twitter.png
   :align: center


Python 3.2 release manager
===========================

- :ref:`python_3_12_0`
