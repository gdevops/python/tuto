.. index::
   ! Mariatta Wijaya

.. _mariatta:

=================================================================================================================
Mariatta Wijaya
=================================================================================================================

- https://x.com/mariatta
- https://mariatta.ca/
- https://github.com/Mariatta
- https://github.com/readme/stories/mariatta-wijaya

Resume
========

🇨🇦🐍 Femail core developer • Sr. DevRel Engineer @Google

• #PyConUS2023 Chair • 🐍🍒⛏️🤖 • You pick the subject, I'll listen to you
• She/her • Picky eater



Hi there wave

My name is Mariatta. My pronoun is she/her/hers. I live in Vancouver
canada and my timezone is UTC-7.

The open source projects that I'm currently focusing on are:

- CPython, specifically the core-workflow, GitHub bots, Devguide and general documentation
- Python Core Sprint
- PyLadies
- gidgethub

I'm also a public speaker and I co-organize Vancouver PyLadies group.

For my contributions to Python, I've received the Community Service Award
from Python Software Foundation.

I've also been nominated twice for Google Open Source Peer Bonus program.

I'm a PSF Fellow member since 2020. In honor of Ada Lovelace Day in 2020,
my story was shared on GitHub ReadME project.
