.. index::
   ! Sébastien Eustace

.. _sebastien_eustace:

=================================================================================================================
Sébastien Eustace
=================================================================================================================

- http://www.sebastien-eustace.fr/
- https://x.com/SDisPater
- https://github.com/SDisPater


Resume
========

Software engineer, proud pythonista, open source lover.
Creator of https://python-poetry.org and https://pendulum.eustace.io
