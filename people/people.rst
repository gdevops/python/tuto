.. index::
   pair: Fedidevs ; Python
   ! People

.. _python_people:

============
People
============

- https://fedidevs.com/python/
- https://fedidevs.com/starter-packs/?q=python 


.. toctree::
   :maxdepth: 3


   gvanrossum/gvanrossum
   carol_willing/carol_willing
   charlie-marsh/charlie-marsh   
   irit_katriel/irit_katriel
   marlene_mhangami/marlene_mhangami
   mariatta_wijaya/mariatta_wijaya
   pablo_galindo_salgado/pablo_galindo_salgado
   sebastien_eustace/sebastien_eustace
   simon-willison/simon-willison
   thomas_wouters/thomas_wouters
