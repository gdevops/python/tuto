.. index::
   ! Pablo Galindo Salgado

.. _pablo_galindo_salgado:

=================================================================================================================
**Pablo Galindo Salgado**
=================================================================================================================


- https://x.com/pyblogsal
- https://discuss.python.org/t/steering-council-nomination-pablo-galindo-salgado-2021-term/5720
- https://www.blog.pythonlibrary.org/2020/04/06/pydev-of-the-week-pablo-galindo-salgado/#more-9645
- https://devguide.python.org/parser/



.. figure:: images/twitter.png
   :align: center


.. toctree::
   :maxdepth: 3

   actions/actions
