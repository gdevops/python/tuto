.. index::
   ! Charlie Marsh

.. _charlie_marsh:

====================================
**Charlie Marsh**
====================================

- https://x.com/charliermarsh
- https://nitter.poast.org/charliermarsh/rss


Resume
========

Building @astral_sh Ruff, uv, and other high-performance Python tools. 
