.. Tuto python documentation master file, created by
   sphinx-quickstart on Fri May 18 21:18:16 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. figure:: _static/python-logo.png
   :align: center

   https://www.python.org/


|FluxWeb| `RSS <https://gdevops.frama.io/python/tuto/rss.xml>`_

.. _python_tuto:

=====================
**Python** tutorial
=====================

:last stable Python release: :ref:`python_versions:current_python_release`
:last next stable Python release: :ref:`python_versions:next_python_release`
:last dev Python release: :ref:`python_versions:last_python_dev_release`


- https://www.python.org/
- :ref:`genindex`
- https://github.com/python/cpython
- https://github.com/python/cpython/branches
- https://github.com/python/cpython/graphs/contributors
- https://devguide.python.org/
- https://www.python.org/dev/peps/
- https://discuss.python.org/
- https://bugs.python.org/
- https://gitlab.com/gdevops/tuto_python
- https://mail.python.org/archives/
- https://mail.python.org/archives/list/python-announce-list@python.org/
- https://mail.python.org/archives/list/python-committers@python.org/latest
- https://mail.python.org/archives/list/python-dev@python.org/
- https://hub.docker.com/_/python/ (python docker images)
- https://github.com/pyenv/pyenv/commits/master (Simple Python version management)
- https://discuss.python.org/latest
- https://www.pythondiscord.com/
- https://discord.gg/python
- https://planetpython.org/
- https://github.com/trending/python?since=daily&spoken_language_code=
- https://github.com/trending/developers/python?since=daily
- https://news.python.sc/ (Pythonic news)
- `Python weeklies`_
- https://pycoders.com/issues
- https://www.reddit.com/r/Python
- https://discourse.jupyter.org/
- https://discuss.afpy.org/
- http://www.montypython.net/
- https://discu.eu/weekly/python/
- http://docs.quantifiedcode.com/python-anti-patterns/


.. _`Python weeklies`:  https://us2.campaign-archive.com/home/?u=e2e180baf855ac797ef407fc7&id=9e26887fc5



.. toctree::
   :maxdepth: 6

   definition/definition
   people/people
   communaute/communaute
   installation/installation

.. toctree::
   :maxdepth: 4

   distribution/distribution
   tools/tools
   typing/typing
   files/files
   pattern_matching/pattern_matching
   representation/representation
   tutorials/tutorials


.. toctree::
   :maxdepth: 5

   news/news

.. toctree::
   :maxdepth: 4

   versions/versions
