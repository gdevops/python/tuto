.. index::
   pair: Python ; Tutorials

.. _python_tutorials:

==================================
Tutorials
==================================

- https://henryiii.github.io/level-up-your-python/notebooks/0%20Intro.html


The Little Book of Python Anti-Patterns
========================================

- https://github.com/quantifiedcode/python-anti-patterns
- http://docs.quantifiedcode.com/python-anti-patterns/

This is an open-source book of Python anti-patterns and worst practices.
Check out docs/index.rst for more information.

.. note:: This is still (and will always be) a work-in-progress, feel free
   to contribute by suggesting improvements, adding new articles, improving
   existing ones, or translating this into other languages.


Socratica
============

- https://www.youtube.com/c/Socratica/videos
- https://ctrlzblog.com/how-to-learn-django-for-free-in-2022/

`Socratica <https://www.youtube.com/playlist?list=PLi01XoE8jYohWFPpC17Z-wWhPOSuh8Er->`_ is hands-down the best YouTube channel for Python beginners
(in my opinion). The videos are short and well explained.

**It’s the channel I wish I had discovered when I was a beginner**.
I’ve since used it to fill in gaps in my knowledge.

Arjan codes
=============

- https://www.youtube.com/c/ArjanCodes/videos

As you gain experience as a developer and your projects get more complex,
success depends less on what you’re building but how.

Right now, I’m a big fan of Arjan Codes. A few months ago, I was assigned
to build a new tool at work with just one other developer.
I was given a lot of freedom to write the code how I wanted, so it was
important that I wrote clean code.

Complex projects involving data from multiple sources involve a lot of
Python. Arjan’s videos have helped me make more use of Object Orientated
Programming (OOP) and identify code smells.

This video on common Python code smells is a great way to learn a few
Python best practices:
