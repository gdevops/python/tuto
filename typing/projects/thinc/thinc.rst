
.. index::
   pair: Typing ; thinc

.. _thinc:

================================================================================================================
**thinc** crystal_ball A refreshing functional take on deep learning, compatible with your favorite libraries
================================================================================================================


.. seealso::

   - https://github.com/explosion/thinc
   - https://x.com/explosion_ai
   - https://x.com/honnibal
   - https://thinc.ai/
   - https://thinc.ai/docs/usage-type-checking






Annonces
=========

.. seealso::

   - https://x.com/PyTorch/status/1222281229095096324?s=20
   - https://x.com/deliprao/status/1222301223690260480?s=20
   - https://x.com/honnibal/status/1222272108547407873?s=20


Features
===========

🚀 Type checking
-------------------

.. seealso::

   - https://thinc.ai/docs/usage-type-checking


Develop faster and catch bugs sooner with sophisticated type checking.

Trying to pass a 1-dimensional array into a model that expects 2 dimensions ?
That’s a type error. Your editor can pick it up as the code leaves your fingers.

🐍 Awesome config
--------------------

.. seealso::

   - https://thinc.ai/docs/usage-config

Configuration is a major pain for ML.

Thinc lets you describe trees of objects, with references to your own functions,
so you can stop passing around blobs of settings. It's simple, clean, and it
works for both research and production.


🔮 Use any framework
-----------------------

.. seealso::

   - https://thinc.ai/docs/usage-frameworks

Switch between PyTorch, TensorFlow and MXNet models without changing your
application, or even create mutant hybrids using zero-copy array interchange.
