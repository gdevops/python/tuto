
.. index::
   pair: Typing ; django-ninja

.. _pythondjango_ninja:

=================================================================================================
**django-ninja** Fast, Async-ready, Openapi, type hints based framework for building APIs
=================================================================================================

.. seealso::

   - https://django-ninja.rest-framework.com/
   - https://github.com/vitalik/django-ninja/graphs/contributors




Description
=============

**Django Ninja** is a web framework for building APIs with Django and
Python 3.6+ based type hints.

Key features
================

- Easy: Designed to be easy to use and intuitive.
- Fast: Very high performance thanks to Pydantic and async support.
- Fast to code: Type hints and automatic docs let's you focus only on
  business logic.
- Standards-based: Based on the open standards for APIs: OpenAPI
  (previously known as Swagger) and JSON Schema.
- Django friendly: (obviously) have good integration with Django core an ORM.
- Production ready: Used by multiple companies on live projects
  (If you use django-ninja and would like to publish your feedback -
  please email to ppr.vitaly@gmail.com)
