
.. index::
   pair: Typing ; FastAPI

.. _prog_fastapi:

=================================================================================================
FastAPI (FastAPI framework, high performance, easy to learn, fast to code, ready for production)
=================================================================================================

.. seealso::

   - :ref:`framework_fastapi`
