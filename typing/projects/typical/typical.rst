.. index::
   pair: Typing ; typical

.. _typical:

=================================================================================================
**typical**: Fast, simple, & correct data-validation using Python 3 typing.
=================================================================================================


.. seealso::

   - https://github.com/seandstewart/typical/
   - https://python-typical.org/


.. figure:: logo.png
   :align: center


.. figure:: typical.png
   :align: center
