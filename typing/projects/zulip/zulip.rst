
.. index::
   pair: Typing ; zulip

.. _zulip:

=================================================================================================
zulip (Zulip server - powerful open source team chat)
=================================================================================================


.. seealso::

   - https://github.com/zulip/zulip
   - https://blog.zulip.org/2016/10/13/static-types-in-python-oh-mypy/
