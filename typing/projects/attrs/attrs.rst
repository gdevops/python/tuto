
.. index::
   pair: Typing ; attrs

.. _attrs:

=================================================================================================
attrs (Python Classes Without Boilerplate)
=================================================================================================


.. seealso::

   - https://github.com/python-attrs/attrs
   - https://www.attrs.org/en/stable/
   - https://www.revsys.com/tidbits/dataclasses-and-attrs-when-and-why/
