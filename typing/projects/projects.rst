
.. index::
   pair: Python Projects ; Typing

.. _python_projects_typing:

=============================================
Python projects using Python type hinting
=============================================

.. seealso::

   - https://github.com/python-attrs/attrs
   - https://www.revsys.com/tidbits/dataclasses-and-attrs-when-and-why/
   - https://docs.python.org/3.8/library/dataclasses.html
   - https://github.com/tiangolo/fastapi
   - https://github.com/zulip/zulip


.. toctree::
   :maxdepth: 3

   attrs/attrs
   dataclasses/dataclasses
   django_ninja/django_ninja
   fastapi/fastapi
   mathy/mathy
   pycln/pycln
   pydantic/pydantic
   rich/rich
   starlette/starlette
   strawberry/strawberry
   thinc/thinc
   typical/typical
   zulip/zulip
