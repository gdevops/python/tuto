
.. index::
   pair: Typing ; rich

.. _rich:

====================================================================================
**rich** (a Python library for rich text and beautiful formatting in the terminal)
====================================================================================


.. seealso::

   - https://github.com/willmcgugan/rich/
