
.. index::
   pair: Typing ; dataclasses
   ! dataclass

.. _dataclasses:

=================================================================================================
**dataclasses** (Data Classes)
=================================================================================================

- https://docs.python.org/3.8/library/dataclasses.html
- https://www.revsys.com/tidbits/dataclasses-and-attrs-when-and-why/
- https://scribe.bus-hit.me/towards-data-science/all-you-need-to-start-coding-with-data-classes-db421bf78a64
