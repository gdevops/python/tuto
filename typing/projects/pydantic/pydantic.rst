
.. index::
   pair: Typing ; pydantic

.. _pydantic:

===================================================
pydantic (Data parsing using Python type hinting)
===================================================


.. seealso::

   - https://github.com/samuelcolvin/pydantic
