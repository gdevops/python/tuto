
.. index::
   pair: Typing ; mathy

.. _mathy:

=========================================================================================================================
**mathy** A platform for using computer algebra systems to solve math problems step-by-step with reinforcement learning
=========================================================================================================================


.. seealso::

   - https://github.com/justindujardin/mathy/
   - https://mathy.ai/




Features
===========

.. seealso::

   - https://fastapi.tiangolo.com/python-types/

`Python with Type Hints`_: typing hints are used everywhere in Mathy to help
provide rich autocompletion and linting in modern IDEs.


.. _`Python with Type Hints`: https://fastapi.tiangolo.com/python-types/
