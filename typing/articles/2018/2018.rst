

.. _python_typing_2018:

==================================
Python typing 2018
==================================



Florimond
=========

.. seealso::

   - https://blog.florimond.dev/why-i-started-using-python-type-annotations-and-why-you-should-too


Bernat
========


.. seealso::

   - https://www.bernat.tech/the-state-of-type-hints-in-python/
