

.. _python_tools_pyright:

=============================================
**pyright**
=============================================

.. seealso::

   - https://github.com/Microsoft/pyright
   - https://github.com/microsoft/pyright/graphs/contributors




Quality
========

.. seealso::

   - :ref:`vscodium_pyright_project`
