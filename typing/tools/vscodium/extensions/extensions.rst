
.. index::
   pair: VSCodium ; Extensions

.. _vscodium_extensions:

=============================================
VSCodium extensions
=============================================

.. toctree::
   :maxdepth: 3

   pyright/pyright
