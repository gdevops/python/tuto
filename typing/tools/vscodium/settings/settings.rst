
.. index::
   pair: VSCodium Extension; settings

.. _vscoium_settings:

=============================================
VSCodium settings
=============================================

.. seealso::

   - https://www.benjaminpack.com/blog/vs-code-python-pipenv/





.gitignore file
==================


::

    ### Python ###
    # Byte-compiled / optimized / DLL files
    __pycache__/
    *.py[cod]
    .vscode/


How to find the python path
===============================

With poetry
------------

::

    poetry shell
    which python

::

    /home/xxxxxx/.cache/pypoetry/virtualenvs/tuto-python-CXb74PZ_-py3.9/bin/python


With pipenv
----------------

::

    pipenv shell
    which python

::

    /home/xxxxx/.local/share/virtualenvs/intranet-Am_WDykK/bin/python



.vscode/settings.json examples
==================================

::

    .vscode/settings.json
    {

        "files.exclude": {
            "**/.git": true,
            "**/*.pyc": true,
            "**/__pycache__": true
        },
        "python.pythonPath": "/home/xxxxx/.local/share/virtualenvs/intranet-Am_WDykK/bin/python"
    }
