
.. index::
   pair: Python ; Typing

.. _vscodium:

=============================================
VSCodium
=============================================

.. toctree::
   :maxdepth: 3


   settings/settings
   extensions/extensions
