.. index::
   pair: Python ; Typing

.. _python_typing:

==================================
Typing
==================================

- https://typing.readthedocs.io/en/latest/
- https://github.com/python/typing
- https://realpython.com/python-type-checking/
- https://docs.python.org/3/library/typing.html
- http://langa.pl/random/talks/type-checking-prod-applications.pdf
- https://blog.florimond.dev/why-i-started-using-python-type-annotations-and-why-you-should-too
- https://www.bernat.tech/the-state-of-type-hints-in-python/


.. toctree::
   :maxdepth: 3

   peps/peps
   articles/articles
   projects/projects
   tools/tools
   3.7/3.7
