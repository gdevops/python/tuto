

.. _pattern_matching_definitions:
.. _pattern_matching_examples:

==================================
Tutorials
==================================

.. toctree::
   :maxdepth: 3

   brandt_bucher/brandt_bucher
   ahulter/ahulter
   gui_latrova/gui_latrova
   inspired_python/inspired_python
   martin_heinz/martin_heinz
   mathspp/mathspp
   raymond_hettinger/raymond_hettinger
   realpython/realpython
