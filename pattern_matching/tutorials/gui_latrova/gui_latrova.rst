

.. _gui_latrova_pattern:

==============================================================================================
**Python Match Case is more powerful than you think 🐍🕹️** by https://x.com/guilatrova
==============================================================================================


- https://x.com/guilatrova
- https://guicommits.com/python-match-case-examples/


Introduction
==============

Python 3.10 brought the match case syntax which is similar to the
switch case from other languages.

It's just similar though. Python's match case is WAY MORE POWERFUL than
the switch case because it's a Structural Pattern Matching.

You don't know what I mean? I'm going to show you what it can do with examples!


Match Case with conditionals (guards)
=========================================

Not exciting yet? Ok, let's improve it a little.

You can see we are missing many status codes in the previous example.

What if we want to match ranges as:

    <200,
    200-399,
    400-499, and
    >=500?

We can use guards for that:

.. code-block:: python

    from http import HTTPStatus
    import random

    http_status = random.choice(list(HTTPStatus))

    match http_status:
        # 💁‍♂️ Note we don't match a specific value as we use "_" (underscore)
        # 👇✅ Match any value, as long as status is between 200-399
        case _ as status if status >= HTTPStatus.OK and status < HTTPStatus.BAD_REQUEST:
            print(f"✅ Everything is good! {status = }")
            # 👆📤 We took 'status' by using the 'as status' syntax

        # 👇❌ Match any value, as long as status is between 400-499
        case _ as status if status >= HTTPStatus.BAD_REQUEST and status < HTTPStatus.INTERNAL_SERVER_ERROR:
            print(f"❌ You did something wrong! {status = }")

        # 👇💣 Match any value, as long as status is >=500
        case _ as status if status >= HTTPStatus.INTERNAL_SERVER_ERROR:
            print(f"💣 Oops... Is the server down!? {status = }.")

        # 👇❓ Match any value that we didn't catch before (<200)
        case _ as status:
            print(f"❓ No clue what to do with {status = }!")


👆 Note we didn't use any specific value inside our case statements.

We used _ (underscore) to match all because we wanted to check ranges
instead of specific values.

We call "guards" when we validate the matched pattern using an if as we did above.


Match Case lists value, position, and length
===================================================

You can match lists based on values at a specific position and even length!

See some examples below where we match:

- Any list with 3 items by using and extracting these items as vars
- Any list with more than 3 items by using \*_
- Any list starting with a specific value + possible combinations
- Any list starting with a specific value


.. code-block:: python

    baskets = [
        ["apple", "pear", "banana"], # 🍎 🍐 🍌
        ["chocolate", "strawberry"], # 🍫 🍓
        ["chocolate", "banana"], # 🍫 🍌
        ["chocolate", "pineapple"], # 🍫 🍍
        ["apple", "pear", "banana", "chocolate"], # 🍎 🍐 🍌 🍫
    ]

    def resolve_basket(basket: list):
        match basket:

            # 👇 Matches any 3 items
            case [i1, i2, i3]: # 👈 These are extracted as vars and used here 👇
                print(f"Wow, your basket is full with: '{i1}', '{i2}' and '{i3}'")

            # 👇 Matches >= 4 items
            case [_, _, _, *_] as basket_items:
                print(f"Wow, your basket has so many items: {len(basket_items)}")

            # 👇 2 items. First should be 🍫, second should be 🍓 or 🍌
            case ["chocolate", "strawberry" | "banana"]:
                print("This is a superb combination. 🍫 + 🍓|🍌")

            # 👇 2 items. First should be 🍫, second should be 🍍
            case ["chocolate", "pineapple"]:
                print("Eww, really? 🍫 + 🍍 = ?")

            # 👇 Any amount of items starting with 🍫
            case ["chocolate", *_]:
                print("I don't know what you plan but it looks delicious. 🍫")

            # 👇 If nothing matched before
            case _:
                print("Don't be cheap, buy something else")


    for basket in baskets:
        print(f"📥 {basket}")
        resolve_basket(basket)
        print()

Match Case dicts
====================

We can do a lot with dicts!

Let's see many examples with dicts holding str keys and either int or str as their values.

We can match existing keys, value types, and dict length.


.. code-block:: python


    mappings: list[dict[str, str | int]] = [
        {"name": "Gui Latrova", "twitter_handle": "@guilatrova"},
        {"name": "John Doe"},
        {"name": "JOHN DOE"},
        {"name": 123456},
        {"full_name": "Peter Parker"},
        {"full_name": "Peter Parker", "age": 16}
    ]

    def resolve_mapping(mapping: dict[str|int]):
        match mapping:
            # 👇 Matches any
            #    (1) "name" AND any (2) "twitter_handle"
            case {"name": name, "twitter_handle": handle}:
                print(f"😉 Make sure to follow {name} at {handle} to keep learning") # 😉 This is good advice

            # 👇 Matches any
            #    (1) "name" (2) if val is str and (3) it's all UPPER CASED
            case {"name": str() as name} if name == name.upper():
                print(f"😥 Hey, there's no need to shout, {name}!")

            # 👇 Matches any
            #    (1) "name" (2) if val is str. It will fall here whenever the above 👆 doesn't match
            case {"name": str() as name}:
                print(f"👋 Hi {name}!")

            # 👇 Matches any
            #    (1) "name" (2) if val is int.
            case {"name": int()}:
                print("🤖 Are you a robot or what? How can I say your name? ")

            # 👇 Matches any
            #    (1) "full_name" (2) and NOTHING else
            case {"full_name": full_name, **remainder} if not remainder:
                print(f"Thanks mr/ms {full_name}!")

            # 👇 Matches any
            #    (1) "full_name" (2) and ANYTHING else
            case {"full_name": full_name, **remainder}:
                print(f"Just your full name is fine! No need to share {list(remainder.keys())}")


    for mapping in mappings:
        print(f"📥 {mapping}")
        resolve_mapping(mapping)
        print()


Match Case classes instances and props
========================================

The first time I saw:


.. code-block:: python

    class Example:
        ...

    var = Example()

    match var:
        case Example(): # 👈 This syntax is a bit weird
            ...

I thought we could be instantiating the class 😅 which is wrong.

This syntax means: "Instance of type Example with any props."

Above you probably saw we doing that for int() and str(). The logic is the same.

Check a few examples:

- Matching a class instance with the property name equals End
- Matching any instance based on the type
- Matching instances with specific properties set to 0
- Extracting class properties to be used inside the handler


.. code-block:: python

    from dataclasses import dataclass

    @dataclass
    class Move:
        x: int # horizontal
        y: int # vertical

    @dataclass
    class Action:
        name: str

    @dataclass
    class UnknownStep:
        random_value = "Darth Vader riding a monocycle"


    steps = [
        Move(1, 0),
        Move(2, 5),
        Move(0, 5),
        Action("Work"),
        Move(0, 0),
        Action("Rest"),
        Move(0, 0),
        UnknownStep(),
        Action("End"),
    ]


    def resolve_step(step):
        match step:
            # 👇 Match any action that has name = "End"
            case Action(name="End"): # 👈 Note we're not instantiating anything
                print("🔚 Flow finished")

            # 👇 Match any Action type
            case Action():
                print("👏 Good to see you're doing something")

            # 👇 Match any Move with x,y == 0,0
            case Move(0, 0):
                print("💂 You're not really moving, stop pretending")

            # 👇 Match any Move with y = 0
            case Move(x, 0):
                print(f"➡️ You're moving horizontally to {x}")

            # 👇 Match any Move with x = 0
            case Move(0, y):
                print(f"🔝 You're moving vertically to {y}")

            # 👇 Match any Move type
            case Move(x, y):
                print(f"🗺️ You're moving to ({x}, {y})")

            # 👇 When nothing matches
            case _:
                print(f"❓ I've got not idea what you're doing")


    for step in steps:
        print(f"📥 {step}")
        resolve_step(step)
        print()
