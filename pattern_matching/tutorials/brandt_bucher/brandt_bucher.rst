

.. _pattern_matching_brandt_bucher:

=============================================================================================
Brandt Bucher: A Perfect Match The history, design, implementation, and future of Python's.
=============================================================================================

- https://github.com/brandtbucher
- https://www.youtube.com/watch?v=XpxTrDDcpPE
- https://pycon-assets.s3.amazonaws.com/2022/media/presentation_slides/116/2022-04-29T05%3A05%3A28.862819/PyCon_US_2022_Outline.pdf

Python 3.10 was released on October 4th, bringing with it a major
new feature: "structural pattern matching".

As one of the designers of the feature and its principal implementer,
my goal is to introduce you to Python's powerful, dynamic, object-oriented
approach to this long-established functional programming construct,
and to explore ways that you might use structural pattern matching in
your own code.

Along the way, we’ll also dive into the history of the match statement,
the design process behind it, how it actually works, and what we're already
doing to improve it in Python 3.11 and beyond.
