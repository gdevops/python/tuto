.. index::
   pair: pattern matching ; inspired Python

.. _inspired_python:

==================================
inspired Python
==================================

- https://www.inspiredpython.com/
- https://x.com/InspiredPython
- https://www.inspiredpython.com/tips
- https://www.inspiredpython.com/all-articles

.. toctree::
   :maxdepth: 3

   mastering/mastering
   etl_and_dataclasses/etl_and_dataclasses
   path_and_files/path_and_files
