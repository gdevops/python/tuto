.. index::
   pair: pattern matching; mastering

.. _inspired_python_mastering:

==========================================
Mastering Structural Pattern Matching
==========================================

- https://www.inspiredpython.com/course/pattern-matching/mastering-structural-pattern-matching


Introduction
=============

If you’re not familiar with the term **Structural Pattern Matching** then
you are not alone.

It’s a feature that, until about 10-15 years ago, you would not see
outside of functional programming languages. Its use, however, has spread;
today you can find a feature like it in **C#, Swift, and Ruby**.

What was once the preserve of niche languages is now available for you
to try in Python 3.10.
