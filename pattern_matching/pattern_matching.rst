.. index::
   pair: Python ; Matching

.. _patterm_matching:

==================================
Pattern matching (Python 3.10+)
==================================

- https://realpython.com/python310-new-features/#structural-pattern-matching
- https://www.python.org/dev/peps/pep-0636/ (Structural Pattern Matching: Tutorial)
- https://www.python.org/dev/peps/pep-0635/ (Structural Pattern Matching: Motivation and Rationale)
- https://www.python.org/dev/peps/pep-0634/ (Structural Pattern Matching: Specification)
- https://docs.python.org/3.10/whatsnew/3.10.html#pep-634-structural-pattern-matching
- https://github.com/brandtbucher
- https://github.com/dmoisset/patma
- https://x.com/dmoisset
- https://ep2021.europython.eu/media/conference/slides/5tcgQpi-pattern-matching-in-python.pdf
- https://discuss.python.org/t/gauging-sentiment-on-pattern-matching/5770

.. toctree::
   :maxdepth: 3

   tutorials/tutorials
