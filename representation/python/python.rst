
.. _python_repr:

==================================
Python __repr__, __str__
==================================

__repr__
==========

- https://docs.python.org/3/reference/datamodel.html#object.__repr__
- https://docs.python.org/fr/3/reference/datamodel.html#object.__repr__
