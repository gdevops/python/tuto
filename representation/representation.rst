.. index::
   pair: Python ; Representation

.. _python_representatin:

==================================
Python representation
==================================

.. toctree::
   :maxdepth: 3

   python/python
   ipython/ipython
