.. index::
   pair: IPython ; Representation

.. _ipython_rich_ouput:

==================================
**IPython Rich output**
==================================

- https://x.com/fperez_org/status/1390183616857055236?s=20


Rich Output
===============

- https://nbviewer.jupyter.org/github/ipython/ipython/blob/master/examples/IPython%20Kernel/Rich%20Output.ipynb


In Python, objects can declare their textual representation using the __repr__
method. IPython expands on this idea and allows objects to declare other,
rich representations including:

- HTML
- JSON
- PNG
- JPEG
- SVG
- LaTeX

A single object can declare some or all of these representations; all are
handled by IPython's display system. This Notebook shows how you can use
this display system to incorporate a broad range of content into your Notebooks.
